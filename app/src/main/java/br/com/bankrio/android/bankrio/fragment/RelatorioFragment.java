package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.RelatoriosAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.RelatoriosRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.RelatorioEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.RelatoriosModel;
import br.com.bankrio.android.bankrio.obj.RelatoriosPostsObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 25/03/16.
 */
public class RelatorioFragment extends SuperFragment {

    protected View view;
    protected ListView lvRelatorios;
    private RelatoriosAdapter relatoriosAdapter;
    protected EventBus bus;
    protected ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_relatorio, container, false);
        initView();
        return view;
    }


    protected void initView() {
        setTitle("Relatórios");

        bus = EventBus.getDefault();
        lvRelatorios = (ListView)view.findViewById(R.id.lv_relatorios);
        lvRelatorios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onClickItem(position);
            }
        });

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        callAPI();

    }

    protected void onClickItem(int position){
        RelatoriosPostsObj relatoriosPostsObj = (RelatoriosPostsObj)lvRelatorios.getItemAtPosition(position);

        if(relatoriosPostsObj != null && relatoriosPostsObj.attachments != null && relatoriosPostsObj.attachments.length > 0 && !Utils.ehVazio(relatoriosPostsObj.attachments[0].url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(relatoriosPostsObj.attachments[0].url.toString().trim()));
            startActivity(browserIntent);
        }else {
            Toast.makeText(this.getActivity(), "Nenhum arquivo encontrado!", Toast.LENGTH_LONG).show();
        }

    }


    protected void callAPI(){
        Cache relatorios = CacheManager.getRelatorios();
        if(relatorios != null && !Utils.needSync(relatorios.currentTime, JsonType.RELATORIO_REFRESH)){
            Gson gson = new Gson();
            RelatoriosModel obj = gson.fromJson(relatorios.json.toString(), RelatoriosModel.class);

            RelatorioEvent event = new RelatorioEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.obj = obj;
                onEvent(event);
            }

            return;
        }

        refresh();
    }


    /**
     * Retorna um array de relatorios
     * */
    public void onEvent(RelatorioEvent event){
        relatoriosAdapter = new RelatoriosAdapter(getActivity(), Arrays.asList(event.obj.posts));
        lvRelatorios.setAdapter(relatoriosAdapter);
        progress.setVisibility(View.GONE);
        lvRelatorios.setVisibility(View.VISIBLE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }



    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()) {
            case R.id.img_refresh: {
                refresh();
                break;
            }
        }
    }

    public void refresh(){
        RelatoriosRest rest = new RelatoriosRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
            lvRelatorios.setVisibility(View.GONE);
        }
    }


}
