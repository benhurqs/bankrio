package br.com.bankrio.android.bankrio.obj;

import java.util.Objects;

/**
 * Created by Benhur on 27/02/16.
 */
public class DefaultObj {
    public String status;
    public int count;
    public int count_total;
    public int pages;

    public class Query{
        public boolean ignore_sticky_posts;
        public String post_type;
    }
}
