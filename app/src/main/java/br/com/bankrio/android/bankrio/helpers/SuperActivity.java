package br.com.bankrio.android.bankrio.helpers;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 21/02/16.
 */
public class SuperActivity extends AppCompatActivity {


    public void showTitle(String title){
        TextView txtTitle = ((TextView) findViewById(R.id.txt_title));
        txtTitle.setText(title);
        txtTitle.setVisibility(View.VISIBLE);

        ((ImageView) findViewById(R.id.img_logo)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.GONE);
    }

    public void showLogo(){
        ((TextView) findViewById(R.id.txt_title)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_logo)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.GONE);
    }

    public void showRefresh(){
        ((ImageView) findViewById(R.id.img_search)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_refresh)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.VISIBLE);
    }

    public void showActionVideo(){
        ((ImageView) findViewById(R.id.img_search)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_refresh)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.VISIBLE);
    }


    public void showActionSearch(){
        ((ImageView) findViewById(R.id.img_search)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.img_refresh)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.VISIBLE);
    }

    public void hideActionSearch(){
        ((ImageView) findViewById(R.id.img_search)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_refresh)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_icon_video)).setVisibility(View.GONE);
    }

}
