package br.com.bankrio.android.bankrio.comm.webfeeder.marquee;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.internal.Streams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFCoreComm;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.obj.webfeeder.MarqueeObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by benhurqs on 06/07/16.
 */
public class MarqueeRest extends WFCoreComm {

    private ArrayList<MarqueeObj> obj;
    private List<String> names;
    private List<String> urls;
    private int count = 0;
    private Gson gson;

    public MarqueeRest(Context context) {
        super(context, RequestType.GET, null);
        names = Arrays.asList(context.getResources().getStringArray(R.array.marquee_names));
        urls = Arrays.asList(context.getResources().getStringArray(R.array.marquee_urls));

        obj = new ArrayList<MarqueeObj>();
        gson = new Gson();

        if(CacheManager.getMarquee() != null && !Utils.ehVazio(CacheManager.getMarquee().json)){
            MarqueeObj[] marqueeObj = gson.fromJson(CacheManager.getMarquee().json.toString(), MarqueeObj[].class);
            bus.post(marqueeObj);
        }
    }


    @Override
    public boolean enviar() {
        if(count < 0 || count >= names.size() ){
            saveMarquee();
            return false;
        }

        url = urls.get(count).toString();
        return super.enviar();
    }


    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        MarqueeObj marqueeObj = gson.fromJson(json.toString(), MarqueeObj.class);
        marqueeObj.name = names.get(count);
        count++;

        obj.add(marqueeObj);
        enviar();
    }

    private void saveMarquee(){
        if(obj.size() > 0) {
            if(CacheManager.getMarquee() == null || Utils.ehVazio(CacheManager.getMarquee().json)){
                CacheManager.saveMarquee(gson.toJson(obj));
                MarqueeObj[] marqueeObj = gson.fromJson(CacheManager.getMarquee().json.toString(), MarqueeObj[].class);
                bus.post(marqueeObj);
                return;
            }
            CacheManager.saveMarquee(gson.toJson(obj));

        }
    }
}

