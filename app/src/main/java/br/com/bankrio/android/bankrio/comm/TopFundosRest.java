package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.TopFundosEvent;
import br.com.bankrio.android.bankrio.model.TopFundosModel;

/**
 * Created by Benhur on 25/03/16.
 */
public class TopFundosRest extends CoreComm {

    private TopFundosEvent event;

    public TopFundosRest(Context context) {
        super(context, RequestType.GET, R.string.top_fundos_url);
        event = new TopFundosEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        TopFundosModel obj = gson.fromJson(json.toString(), TopFundosModel.class);

        if(obj.posts.length > 0) {
            event.obj = obj;
            bus.post(event);

            CacheManager.saveTopFundos(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}