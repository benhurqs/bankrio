package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.BlogActivity;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 19/02/16.
 */
public class VitrineSlideFragment extends Fragment {

    public BlogPostObj destaque;
    private TextView txtCategoria, txttitle;
    private ImageView imgBackground;
    private View view;
    public int position;
    private String[] titles, descriptions;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.item_vitrine_home, container, false);

        view = rootView;
        initView();
        return rootView;
    }

    public void initView(){

        txtCategoria = (TextView)view.findViewById(R.id.txt_vitrine_categoria);
        txttitle = (TextView)view.findViewById(R.id.txt_vitrine_title);
        imgBackground = (ImageView) view.findViewById(R.id.img_bg);

        managerSlides(position);

//        if(position == 5 && destaque != null){
//            getDestaque();
//        }

    }

    private void managerSlides(int position){
        titles = getResources().getStringArray(R.array.titles);
        descriptions = getResources().getStringArray(R.array.descriptions);

        if(position < 5){
            setDescriptions(descriptions[position],titles[position]);
        }else if(position == 5 && destaque != null){
            getDestaque();
        }


    }

    private void getDestaque(){

            if(destaque.categories != null && destaque.categories.length > 0) {
                txtCategoria.setText(Html.fromHtml(destaque.categories[0].title));
            }

            if(destaque.title != null){
                txttitle.setText(Html.fromHtml(destaque.title));
            }

            if(destaque.attachments != null && destaque.attachments.length > 0
                    && destaque.attachments[0].images != null
                    && destaque.attachments[0].images.full != null) {

                Glide.with(view.getContext())
                        .load(destaque.attachments[0].images.full.url)
                        .into(imgBackground);

            }

        imgBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent blog = new Intent(getActivity(), BlogActivity.class);
                blog.putExtra(Utils.BLOG_OBJECT, destaque);
                getActivity().startActivity(blog);
            }
        });

    }

    private void setDescriptions(String title, String descriptions){
        txttitle.setText(Html.fromHtml(title));
        txtCategoria.setText(Html.fromHtml(descriptions));
    }




}
