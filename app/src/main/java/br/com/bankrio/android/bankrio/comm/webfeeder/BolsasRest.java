package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.obj.webfeeder.BolsasObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 07/06/16.
 */
public class BolsasRest extends WFCoreComm {

    public BolsasRest(Context context) {
        super(context, RequestType.GET, R.string.wf_bolsas_url);
    }

    @Override
    public boolean enviar() {
        Cache bolsas = CacheManager.getBolsas();
        if(bolsas != null && !Utils.needSync(bolsas.currentTime, JsonType.WF_REFRESH)) {
            sendResponse(bolsas.json);
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        BolsasObj[] obj = gson.fromJson(json.toString(), BolsasObj[].class);


        if(obj.length > 0) {
            bus.post(obj);
            CacheManager.saveBolsas(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
