package br.com.bankrio.android.bankrio.cache;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

/**
 * Created by Benhur on 20/03/16.
 */
public class CacheManager {

    /******************************** BLOG ***************************************/

    public static void saveBlog(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.BLOG).execute();

        //Save new json
        saveCache(json, JsonType.BLOG);
    }

    public static Cache getBlog(){
        return selectItemByType(JsonType.BLOG);
    }


    /******************************** Categorias ***************************************/

    public static void saveCategories(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.CATEGORIAS).execute();

        //Save new json
        saveCache(json, JsonType.CATEGORIAS);
    }

    public static Cache getCategories(){
        return selectItemByType(JsonType.CATEGORIAS);
    }


    /******************************** Videos ***************************************/

    public static void saveVideos(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.VIDEO).execute();

        //Save new json
        saveCache(json, JsonType.VIDEO);
    }

    public static Cache getVideos(){
        return selectItemByType(JsonType.VIDEO);
    }


    /******************************** Renda Fixa ***************************************/

    public static void saveRendaFixa(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.RENDAFIXA).execute();

        //Save new json
        saveCache(json, JsonType.RENDAFIXA);
    }

    public static Cache getRendaFixa(){
        return selectItemByType(JsonType.RENDAFIXA);
    }

    /******************************** Relatorios ***************************************/

    public static void saveRelatorios(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.RELATORIO).execute();

        //Save new json
        saveCache(json, JsonType.RELATORIO);
    }

    public static Cache getRelatorios(){
        return selectItemByType(JsonType.RELATORIO);
    }


    /******************************** Formularios ***************************************/

    public static void saveFormularios(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.FORMULARIO).execute();

        //Save new json
        saveCache(json, JsonType.FORMULARIO);
    }

    public static Cache getFormularios(){
        return selectItemByType(JsonType.FORMULARIO);
    }

    /******************************** Top Fundos ***************************************/

    public static void saveTopFundos(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.TOP_FUNDOS).execute();

        //Save new json
        saveCache(json, JsonType.TOP_FUNDOS);
    }

    public static Cache getTopFundos(){
        return selectItemByType(JsonType.TOP_FUNDOS);
    }

    /******************************** Formularios ***************************************/

    public static void saveProfissionais(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.PROFISSIONAIS).execute();

        //Save new json
        saveCache(json, JsonType.PROFISSIONAIS);
    }

    public static Cache getProfissionais(){
        return selectItemByType(JsonType.PROFISSIONAIS);
    }

    /******************************** SOBRE ***************************************/

    public static void saveSobre(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.SOBRE).execute();

        //Save new json
        saveCache(json, JsonType.SOBRE);
    }

    public static Cache getSobre(){
        return selectItemByType(JsonType.SOBRE);
    }


    /******************************** FALE CONOSCO ***************************************/

    public static void saveEscritorios(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.ESCRITORIOS).execute();

        //Save new json
        saveCache(json, JsonType.ESCRITORIOS);
    }

    public static Cache getEscritorios(){
        return selectItemByType(JsonType.ESCRITORIOS);
    }

    /******************************** WEBFEDER SIGN IN ***************************************/

    public static void saveSignIn(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_SIGNIN).execute();

        //Save new json
        saveCache(json, JsonType.WF_SIGNIN);
    }

    public static Cache getSignIn(){
        return selectItemByType(JsonType.WF_SIGNIN);
    }

    /******************************** WEBFEDER CAMBIO ***************************************/

    public static void saveCambio(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_CAMBIO).execute();

        //Save new json
        saveCache(json, JsonType.WF_CAMBIO);
    }

    public static Cache getCambio(){
        return selectItemByType(JsonType.WF_CAMBIO);
    }

    /******************************** WEBFEDER ACOES ***************************************/

    public static void saveAcoes(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_ACOES).execute();

        //Save new json
        saveCache(json, JsonType.WF_ACOES);
    }

    public static Cache getAcoes(){
        return selectItemByType(JsonType.WF_ACOES);
    }

    /******************************** WEBFEDER BOLSAS ***************************************/

    public static void saveBolsas(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_BOLSAS).execute();

        //Save new json
        saveCache(json, JsonType.WF_BOLSAS);
    }

    public static Cache getBolsas(){
        return selectItemByType(JsonType.WF_BOLSAS);
    }

    /******************************** WEBFEDER COMMODITIES ***************************************/

    public static void saveCommodities(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_COMMODITIES).execute();

        //Save new json
        saveCache(json, JsonType.WF_COMMODITIES);
    }

    public static Cache getCommodities(){
        return selectItemByType(JsonType.WF_COMMODITIES);
    }

    /******************************** WEBFEDER INDICADORES ***************************************/

    public static void saveIndicadores(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_INDICADORES).execute();

        //Save new json
        saveCache(json, JsonType.WF_INDICADORES);
    }

    public static Cache getIndicadores(){
        return selectItemByType(JsonType.WF_INDICADORES);
    }

    /******************************** WEBFEDER INFLACAO ***************************************/

    public static void saveInflacao(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_INFLACAO).execute();

        //Save new json
        saveCache(json, JsonType.WF_INFLACAO);
    }

    public static Cache getInflacao(){
        return selectItemByType(JsonType.WF_INFLACAO);
    }



    /******************************** WEBFEDER MARQUEE ***************************************/

    public static void saveMarquee(String json){
        //Delete last saved json
        new Delete().from(Cache.class).where("type = ?", JsonType.WF_MARQUEE).execute();

        //Save new json
        saveCache(json, JsonType.WF_MARQUEE);
    }

    public static Cache getMarquee(){
        return selectItemByType(JsonType.WF_MARQUEE);
    }


    /**********************************************************************************/




    private static void saveCache(String json, int type){
        Cache cache  = new Cache();
        cache.json = json;
        cache.type = type;
        cache.currentTime = System.currentTimeMillis();
        cache.save();
    }

    public static Cache selectItemByType(int type) {
        return new Select()
                .from(Cache.class)
                .where("type = ?", type)
                .executeSingle();
    }
}
