package br.com.bankrio.android.bankrio.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 17/12/15.
 */
public class Utils {
    public static final String BLOG_OBJECT = "blog_object";
    public static final String YoutuBE_OBJECT = "youtube_object";

    private static String TAG = "BankRio";
    private static Boolean debug = true;

    public static void dLog(String msg){
        if(debug){
            Log.e(TAG, msg);
        }
    }

    public static boolean ehVazio(String string) {
        return (string==null || "".equals(string.trim()) || "null".equals(string.toLowerCase()));
    }

    private static ConnectivityManager getConnectivityManager(Context ctx) {
        return (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     * Verifica se o aparelho tem conexao
     */
    public static boolean isDeviceOnline(Context ctx) {
        ConnectivityManager cm = getConnectivityManager(ctx);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        }
        return ni.isConnected();
    }

    public static String formatValue(String valor){
        if(!ehVazio(valor)){
            return "R$ " + valor;
        }

        return "";
    }

    public static String formatValue(double value){
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        return formatValue(formatter.format(value/100));
    }

    public static boolean isHD(Context ctx){
        if(ctx == null){
            return false;
        }

        // return 0.75 if it's LDPI
        // return 1.0 if it's MDPI
        // return 1.5 if it's HDPI
        // return 2.0 if it's XHDPI
        if(ctx.getResources().getDisplayMetrics().density > 1.0){
           return true;
        }
        return false;
    }

    public static void showAlertDialog(Activity activity, String msg){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(msg);
         builder.setPositiveButton("OK", null);//second parameter used for onclicklistener
        builder.show();
    }

    public static boolean needSync(long time, int mintoRefresh){
        Calendar today = Calendar.getInstance();
        if(time == 0){
            return true;
        }

        long diff = today.getTimeInMillis() - time;
        long min = diff / ( 60 * 1000);
        long days = diff / (24 * 60 * 60 * 1000);

        boolean validad = false;

        if(Utils.debug){
            //Quando for em modo debug atualiza de 5 em 5 minutos
            validad = (min >= mintoRefresh);
        }else{
            validad = (days >= 1);
        }

        if(validad){
            return true;
        }

        return false;
    }

    public static String getDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(new Date());
    }

    public static String getIntervalDate(){
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Calendar before30 = Calendar.getInstance();
        before30.add(Calendar.DATE, -30);


        Date nowDate = now.getTime();
        Date before30Date = before30.getTime();

        return sdf.format(before30Date).concat("0000").concat("/").concat(sdf.format(nowDate)).concat("230000");
    }

    public static String convertDate(String date){
//        May 12, 2016 12:00:00 AM
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
            Utils.dLog("Error ==>  " + ex.getMessage() + "  -  " + date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        if(testDate == null){
            return "error";
        }
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    public static String convertHour(String date){
//        May 12, 2016 12:00:00 AM
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
            Utils.dLog("Error ==>  " + ex.getMessage() + "  -  " + date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("hh/mm");
        if(testDate == null){
            return "error";
        }
        String newFormat = formatter.format(testDate);
        return newFormat.replace("/","h");
    }

    public static String convertDateTimeToDate(String date){
        //        "17-06-2016 00:23:05"
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
            Utils.dLog("Error ==>  " + ex.getMessage() + "  -  " + date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        if(testDate == null){
            return "error";
        }
        String newFormat = formatter.format(testDate);
        return newFormat;
    }
}
