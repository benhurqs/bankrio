package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.YoutubePlayerActivity;
import br.com.bankrio.android.bankrio.obj.VideoPostObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 27/02/16.
 */
public class VideosAdapter extends BaseAdapter {
    private VideoPostObj[] videos;
    private LayoutInflater inflater;
    private Context context;

    public VideosAdapter(Context context, VideoPostObj[] videos) {
        this.videos = videos;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return videos.length;
    }

    @Override
    public VideoPostObj getItem(int position) {
        return videos[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final VideoPostObj videoObj = getItem(position);

        view = inflater.inflate(R.layout.item_video,null);

        TextView txtTitle = (TextView)view.findViewById(R.id.txt_video_title);
        txtTitle.setText(videoObj.title);

        ImageView thumb = (ImageView)view.findViewById(R.id.img_thumb);
        thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent youtube = new Intent(context, YoutubePlayerActivity.class);
                if(videoObj.custom_fields.codigo_do_video != null && videoObj.custom_fields.codigo_do_video.length > 0 && videoObj.custom_fields.codigo_do_video[0] != null) {
                    youtube.putExtra(Utils.YoutuBE_OBJECT, videoObj.custom_fields.codigo_do_video[0]);
                }
                context.startActivity(youtube);
            }
        });

        if(videoObj.attachments != null && videoObj.attachments.length > 0
                && videoObj.attachments[0].images != null
                && videoObj.attachments[0].images.thumbnail != null) {

            Glide.with(view.getContext())
                    .load(videoObj.attachments[0].images.thumbnail.url)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(thumb);
        }else{
            Glide.with(view.getContext())
                    .load(R.drawable.placeholder)
                    .into(thumb);
        }

        return view;
    }
}