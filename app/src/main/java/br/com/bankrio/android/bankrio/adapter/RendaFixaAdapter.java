package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.RendaFixaPostsObj;

/**
 * Created by Benhur on 20/03/16.
 */
public class RendaFixaAdapter extends BaseAdapter {
    private List<RendaFixaPostsObj> rendas;
    private LayoutInflater inflater;
    private Context context;


    public RendaFixaAdapter(Context context, List<RendaFixaPostsObj> rendas) {
        this.rendas = rendas;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return rendas.size();
    }

    @Override
    public RendaFixaPostsObj getItem(int position) {
        return rendas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final RendaFixaPostsObj rendaPostObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_renda_fixa,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(rendaPostObj.custom_fields != null && rendaPostObj.custom_fields.nome_banco_renda_fixa != null && rendaPostObj.custom_fields.nome_banco_renda_fixa.length > 0) {
            TextView txtBanco = (TextView) view.findViewById(R.id.txt_nome_banco);
            txtBanco.setText(Html.fromHtml(rendaPostObj.custom_fields.nome_banco_renda_fixa[0]));
        }

        if(rendaPostObj.title != null){
            TextView txtFundo = (TextView) view.findViewById(R.id.txt_nome_fundo);
            txtFundo.setText(Html.fromHtml(rendaPostObj.title));
        }

        if(rendaPostObj.custom_fields != null && rendaPostObj.custom_fields.cdi_renda_fixa != null && rendaPostObj.custom_fields.cdi_renda_fixa.length > 0) {
            TextView txtCdi = (TextView) view.findViewById(R.id.txt_cdi);
            txtCdi.setText(Html.fromHtml(rendaPostObj.custom_fields.cdi_renda_fixa[0]) + "% CDI");
        }

        if(rendaPostObj.custom_fields != null && rendaPostObj.custom_fields.prazo_renda_fixa != null && rendaPostObj.custom_fields.prazo_renda_fixa.length > 0) {
            TextView txtCalendar = (TextView) view.findViewById(R.id.txt_calendar);
            txtCalendar.setText(Html.fromHtml(rendaPostObj.custom_fields.prazo_renda_fixa[0]) + " DIAS");
        }

        if(rendaPostObj.custom_fields != null && rendaPostObj.custom_fields.valor_minimo != null && rendaPostObj.custom_fields.valor_minimo.length > 0) {
            TextView txtValor = (TextView) view.findViewById(R.id.txt_value);
            txtValor.setText(Html.fromHtml(rendaPostObj.custom_fields.valor_minimo[0]) + ",00");
        }

        return view;
    }
}
