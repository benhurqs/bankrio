package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.TopFundosAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.TopFundosRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.TopFundosEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.TopFundosModel;
import br.com.bankrio.android.bankrio.obj.TopFundosPostsObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 25/03/16.
 */
public class TopFundosFragment extends SuperFragment {

    protected View view;
    protected ListView lvFundos;
    private TopFundosAdapter fundosAdapter;
    protected EventBus bus;
    protected ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_top_fundos, container, false);
        initView();
        return view;
    }


    protected void initView() {
        setTitle("Top Fundos");

        bus = EventBus.getDefault();
        lvFundos = (ListView)view.findViewById(R.id.lv_fundos);
        lvFundos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onClickItem(position);
            }
        });

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        callAPI();

    }


    protected void callAPI(){
        Cache fundos = CacheManager.getTopFundos();
        if(fundos != null && !Utils.needSync(fundos.currentTime, JsonType.TOP_FUNDOS_REFRESH)){
            Gson gson = new Gson();
            TopFundosModel obj = gson.fromJson(fundos.json.toString(), TopFundosModel.class);

            TopFundosEvent event = new TopFundosEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.obj = obj;
                onEvent(event);
            }

            return;
        }

        refresh();
    }


    /**
     * Retorna um array de Top Fundos
     * */
    public void onEvent(TopFundosEvent event){
        fundosAdapter = new TopFundosAdapter(getActivity(), Arrays.asList(event.obj.posts));
        lvFundos.setAdapter(fundosAdapter);
        progress.setVisibility(View.GONE);
        lvFundos.setVisibility(View.VISIBLE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }



    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()) {
            case R.id.img_refresh: {
                refresh();
                break;
            }
        }
    }

    public void refresh(){
        TopFundosRest rest = new TopFundosRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
            lvFundos.setVisibility(View.GONE);
        }
    }

    private void onClickItem(int position){

        TopFundosPostsObj topFundosPostObj = (TopFundosPostsObj)lvFundos.getItemAtPosition(position);

        if(topFundosPostObj != null && topFundosPostObj.attachments != null && topFundosPostObj.attachments.length > 0 && !Utils.ehVazio(topFundosPostObj.attachments[0].url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(topFundosPostObj.attachments[0].url.toString().trim()));
            startActivity(browserIntent);
        }else {
            Toast.makeText(this.getActivity(), "Nenhum arquivo encontrado!", Toast.LENGTH_LONG).show();
        }
    }


}
