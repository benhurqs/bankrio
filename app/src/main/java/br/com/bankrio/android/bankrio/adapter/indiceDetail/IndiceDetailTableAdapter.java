package br.com.bankrio.android.bankrio.adapter.indiceDetail;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.webfeeder.DetailObj;

/**
 * Created by Benhur on 31/05/16.
 */
public class IndiceDetailTableAdapter extends BaseAdapter {
    private List<DetailObj> cambioList;
    private LayoutInflater inflater;
    private Context context;


    public IndiceDetailTableAdapter(Context context, List<DetailObj> cambios) {
        this.cambioList = cambios;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return cambioList.size();
    }

    @Override
    public DetailObj getItem(int position) {
        return cambioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final DetailObj rendaPostObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_indice_detail,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        TextView txtData = (TextView) view.findViewById(R.id.txt_data);
        txtData.setText(getValue(rendaPostObj.timeTrade));

        TextView txtVariacao = (TextView) view.findViewById(R.id.txt_variacao);
        txtVariacao.setText(Html.fromHtml(String.format("%.2f", rendaPostObj.change())) + "%");

        TextView txtCotacao = (TextView) view.findViewById(R.id.txt_cotacao);
        txtCotacao.setText("R$" + Html.fromHtml(String.valueOf(rendaPostObj.open).replace(".", ",")));

        TextView txtMin = (TextView) view.findViewById(R.id.txt_minimo);
        txtMin.setText("R$" + Html.fromHtml(String.valueOf(rendaPostObj.low).replace(".", ",")));

        ImageView imgVariação = (ImageView)view.findViewById(R.id.img_variacao);
        if(rendaPostObj.change() < 0){
            imgVariação.setImageResource(R.drawable.ic_ind_down);
        }else{
            imgVariação.setImageResource(R.drawable.ic_ind_up);
        }


        return view;
    }

    protected String getValue(String date){
        return null;
    }


}