package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.BlogActivity;
import br.com.bankrio.android.bankrio.adapter.BlogAdapter;
import br.com.bankrio.android.bankrio.adapter.CategoriasAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.BlogRest;
import br.com.bankrio.android.bankrio.comm.CategoriasRest;
import br.com.bankrio.android.bankrio.events.BlogEvent;
import br.com.bankrio.android.bankrio.events.CategoriasEvent;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.BlogModel;
import br.com.bankrio.android.bankrio.model.CategoriasModel;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.obj.CategoriaObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 21/02/16.
 */
public class BlogFragment extends SuperFragment implements AdapterView.OnItemClickListener {

    private View view;
    private ListView lvCategorias, lvBlog;
    private TextView txtMore;
    private CategoriasAdapter categoriasAdapter ;
    private BlogAdapter blogAdapter;
    private EventBus bus;
    private ProgressBar progress;
    private ArrayList<CategoriaObj> categorias;
    private BlogPostObj[] blogPostObj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_blog, container, false);
        initView();
        return view;
    }


    private void initView() {
        setTitle("Blog");

        bus = EventBus.getDefault();
        lvCategorias = (ListView)view.findViewById(R.id.lv_categorias);
        lvCategorias.setOnItemClickListener(this);

        lvBlog = (ListView)view.findViewById(R.id.lv_blog);
        lvBlog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(blogPostObj.length > position) {
                    BlogPostObj b = (BlogPostObj)lvBlog.getItemAtPosition(position);

                    Intent blog = new Intent(getActivity(), BlogActivity.class);
                    blog.putExtra(Utils.BLOG_OBJECT, b);
                    getActivity().startActivity(blog);
                }
            }
        });
        txtMore = (TextView)view.findViewById(R.id.txt_more);

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        callCategoriasAPI();
        callBlogAPI();

    }

    private void callCategoriasAPI(){
        Cache categories = CacheManager.getCategories();
        if(categories != null && !Utils.needSync(categories.currentTime, JsonType.CATEGORIA_REFRESH)){
            Gson gson = new Gson();
            CategoriasModel obj = gson.fromJson(categories.json.toString(), CategoriasModel.class);

            CategoriasEvent event = new CategoriasEvent();

            if(obj.categories != null && obj.categories.length > 0) {
                event.categorias = obj.categories;
                onEvent(event);
            }

            return;
        }


        CategoriasRest rest = new CategoriasRest(getActivity());
        if(rest.enviar()){
        }
    }

    private void callBlogAPI(){
        Cache blog = CacheManager.getBlog();
        if(blog != null && !Utils.needSync(blog.currentTime, JsonType.BLOG_REFRESH)){
            Gson gson = new Gson();
            BlogModel obj = gson.fromJson(blog.json.toString(), BlogModel.class);

            BlogEvent event = new BlogEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.blogs = obj;
                onEvent(event);
            }

            return;
        }

        BlogRest rest = new BlogRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Retorna um array de categorias
     * */
    public void onEvent(CategoriasEvent event){
        categorias = new ArrayList<CategoriaObj>();
        //add Todas Categorias
        CategoriaObj todas = new CategoriaObj();
        todas.id = 0;
        todas.title = "Todas Categorias";

        categorias.add(todas);

        for(CategoriaObj obj : event.categorias){
            categorias.add(obj);
        }

        categoriasAdapter = new CategoriasAdapter(getActivity(),categorias);
        lvCategorias.setAdapter(categoriasAdapter);
    }

    /**
     * Retorna um array de blogs
     * */
    public void onEvent(BlogEvent event){
        blogPostObj = event.blogs.posts;
        blogAdapter = new BlogAdapter(getActivity(), Arrays.asList(event.blogs.posts));
        lvBlog.setAdapter(blogAdapter);
        progress.setVisibility(View.GONE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }

    private void managerCategorias(){
        if(lvCategorias.getVisibility() == View.VISIBLE){
            txtMore.setText("+");
            lvCategorias.setVisibility(View.GONE);
        }else{
            txtMore.setText("-");
            lvCategorias.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()) {
            case R.id.rl_categorias: {
                managerCategorias();
                break;
            }
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        managerCategorias();
        CategoriaObj categoria = (CategoriaObj) parent.getAdapter().getItem(position);
        if(categoria == null){
            return;
        }

        if (position != 0) {
            List<BlogPostObj> blogPosts = new ArrayList<BlogPostObj>();
            for(BlogPostObj blog : blogPostObj){
                if(blog.belongsToCategory(categoria.id)){
                    blogPosts.add(blog);
                }
            }

            blogAdapter = new BlogAdapter(getActivity(),blogPosts);
            lvBlog.setAdapter(blogAdapter);
        }else{
            blogAdapter = new BlogAdapter(getActivity(),Arrays.asList(blogPostObj));
            lvBlog.setAdapter(blogAdapter);
        }

    }
}