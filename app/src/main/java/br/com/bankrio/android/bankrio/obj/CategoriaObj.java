package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

/**
 * Created by Benhur on 27/02/16.
 */
public class CategoriaObj implements Serializable {

    public int id;
    public String slug;
    public String title;
    public String description;
    public int parent;
    public int post_count;
}
