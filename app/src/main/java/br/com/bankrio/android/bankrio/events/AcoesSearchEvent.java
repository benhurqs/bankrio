package br.com.bankrio.android.bankrio.events;

import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;

/**
 * Created by benhurqs on 12/07/16.
 */
public class AcoesSearchEvent {
    public CambioObj acao;
    public String error;
}
