package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

/**
 * Created by Benhur on 27/02/16.
 */
public class ImagesSizeObj implements Serializable {
    public String url;
    public int width;
    public int height;
}
