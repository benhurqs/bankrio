package br.com.bankrio.android.bankrio.fragment.indices.detail;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.indiceDetail.IndiceDetailSlideAdapter;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.utils.MoedasNameUtils;

/**
 * Created by Benhur on 31/05/16.
 */
public class IndiceDetailFragment extends SuperFragment {

    private View view;
    private String name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indice_detail, container, false);
        initView();
        return view;
    }

    private void initView(){
        if(getArguments() != null) {
            name = getArguments().getString("name");
        }
        if(name != null){
            setTitle(MoedasNameUtils.getName(name));
        }else {
            setTitle(name);
        }

        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) view.findViewById(R.id.viewpager);
        pager.setAdapter(new IndiceDetailSlideAdapter(getActivity(),getActivity().getSupportFragmentManager(), this.getArguments()));

        // Custom tab bar
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        tabs.setTextColor(getActivity().getResources().getColor(R.color.white));
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);

    }



}
