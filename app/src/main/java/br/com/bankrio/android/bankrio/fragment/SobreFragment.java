package br.com.bankrio.android.bankrio.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.SobreSlidePagerAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.SobreRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.SobreEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.SobreModel;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 21/02/16.
 */
public class SobreFragment extends SuperFragment {

    private View view;
    private TextView txtTitle1, txtTitle2, txtTitle3, txtTitle4, txtTitle5, txtTitle6;
    private RelativeLayout rlPasso1, rlPasso2, rlPasso3, rlPasso4, rlPasso5, rlPasso6;
    private TextView txtPasso1, txtPasso2, txtPasso3, txtPasso4, txtPasso5, txtPasso6, txtIntroducao, txtComoFazemos;
    private ImageView imgArrow1,imgArrow2,imgArrow3,imgArrow4,imgArrow5,imgArrow6;
    private ViewPager mPagerSobre;
    private SobreSlidePagerAdapter mPagerSobreAdapter;
    private EventBus bus;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_sobre, container, false);
        initView();
        return view;
    }

    private int position = 0;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if( position >= SobreSlidePagerAdapter.NUM_PAGES){
                position = 0;
            }else{
                position = position+1;
            }
            mPagerSobre.setCurrentItem(position, true);
            handler.postDelayed(runnable, SobreSlidePagerAdapter.SLIDE_TIME);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
        if (handler!= null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        handler.postDelayed(runnable, SobreSlidePagerAdapter.SLIDE_TIME);
        bus.register(this);
    }


    private void initView() {
        setTitle("Sobre");
        bus = EventBus.getDefault();

        // Instantiate a ViewPager and a PagerAdapter.
        mPagerSobre = (ViewPager) view.findViewById(R.id.vp_slide_sobre);
        mPagerSobreAdapter = new SobreSlidePagerAdapter(getActivity().getSupportFragmentManager());
        mPagerSobre.setAdapter(mPagerSobreAdapter);

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        txtIntroducao = (TextView)view.findViewById(R.id.txt_introcucao);
        txtComoFazemos = (TextView)view.findViewById(R.id.txt_como_fazemos);

        rlPasso1 = (RelativeLayout)view.findViewById(R.id.ll_passo1);
        rlPasso2 = (RelativeLayout)view.findViewById(R.id.ll_passo2);
        rlPasso3 = (RelativeLayout)view.findViewById(R.id.ll_passo3);
        rlPasso4 = (RelativeLayout)view.findViewById(R.id.ll_passo4);
        rlPasso5 = (RelativeLayout)view.findViewById(R.id.ll_passo5);
        rlPasso6 = (RelativeLayout)view.findViewById(R.id.ll_passo6);

        txtPasso1 = (TextView)view.findViewById(R.id.txt_descricao1);
        txtPasso2 = (TextView)view.findViewById(R.id.txt_descricao2);
        txtPasso3 = (TextView)view.findViewById(R.id.txt_descricao3);
        txtPasso4 = (TextView)view.findViewById(R.id.txt_descricao4);
        txtPasso5 = (TextView)view.findViewById(R.id.txt_descricao5);
        txtPasso6 = (TextView)view.findViewById(R.id.txt_descricao6);

        txtTitle1 = (TextView)view.findViewById(R.id.txt_title_1);
        txtTitle2 = (TextView)view.findViewById(R.id.txt_title_2);
        txtTitle3 = (TextView)view.findViewById(R.id.txt_title_3);
        txtTitle4 = (TextView)view.findViewById(R.id.txt_title_4);
        txtTitle5 = (TextView)view.findViewById(R.id.txt_title_5);
        txtTitle6 = (TextView)view.findViewById(R.id.txt_title_6);

        imgArrow1 = (ImageView)view.findViewById(R.id.img_arrow1);
        imgArrow2 = (ImageView)view.findViewById(R.id.img_arrow2);
        imgArrow3 = (ImageView)view.findViewById(R.id.img_arrow3);
        imgArrow4 = (ImageView)view.findViewById(R.id.img_arrow4);
        imgArrow5 = (ImageView)view.findViewById(R.id.img_arrow5);
        imgArrow6 = (ImageView)view.findViewById(R.id.img_arrow6);

        callAPISobre();
    }

    private void callAPISobre(){
        Cache sobre = CacheManager.getSobre();
        if(sobre != null && !Utils.needSync(sobre.currentTime, JsonType.SOBRE_REFRESH)){
            Gson gson = new Gson();
            SobreModel obj = gson.fromJson(sobre.json.toString(), SobreModel.class);

            SobreEvent event = new SobreEvent();

            if(obj.page != null) {
                event.obj = obj;
                onEvent(event);
            }

            return;
        }


        SobreRest rest = new SobreRest(this.getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
        }
    }

    private void showDescription(RelativeLayout layout, TextView description, ImageView imgArrow){
        if(description.getVisibility() == View.VISIBLE){
            layout.setBackgroundResource((R.drawable.box_sobre));
            description.setVisibility(View.GONE);
            imgArrow.setImageResource(R.drawable.ic_arrow_close);
        }else{
            layout.setBackgroundResource((R.drawable.box_sobre_selected));
            description.setVisibility(View.VISIBLE);
            imgArrow.setImageResource(R.drawable.ic_arrow_open);
        }
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()){
            case R.id.ll_passo1: {
                showDescription(rlPasso1, txtPasso1, imgArrow1);
                break;
            }
            case R.id.ll_passo2: {
                showDescription(rlPasso2, txtPasso2, imgArrow2);
                break;
            }
            case R.id.ll_passo3:{
                showDescription(rlPasso3, txtPasso3, imgArrow3);
                break;
            }
            case R.id.ll_passo4: {
                showDescription(rlPasso4, txtPasso4, imgArrow4);
                break;
            }
            case R.id.ll_passo5: {
                showDescription(rlPasso5, txtPasso5, imgArrow5);
                break;
            }
            case R.id.ll_passo6:{
                showDescription(rlPasso6, txtPasso6, imgArrow6);
                break;
            }
            default:
                break;

        }
    }

    /**
     * Retorna os textos
     * */
    public void onEvent(SobreEvent event){
        progress.setVisibility(View.GONE);
        if(event == null || event.obj == null){
            return;
        }

        populateData(event.obj);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }

    private void populateData(SobreModel obj){
        if(obj.page == null){
            return;
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.texto_introducao != null && obj.page.custom_fields.texto_introducao.length > 0){
            txtIntroducao.setText(Html.fromHtml(obj.page.custom_fields.texto_introducao[0].replace("\r\n","<br>")));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.texto_comofazemos != null && obj.page.custom_fields.texto_comofazemos.length > 0){
            txtComoFazemos.setText(Html.fromHtml(obj.page.custom_fields.texto_comofazemos[0].replace("\r\n","<br>")));
        }

        /************** Passos *******************/

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_0_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_0_numero_de_exibição.length > 0){
            txtTitle1.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_0_numero_de_exibição[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_1_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_1_numero_de_exibição.length > 0){
            txtTitle2.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_1_numero_de_exibição[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_2_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_2_numero_de_exibição.length > 0){
            txtTitle3.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_2_numero_de_exibição[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_3_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_3_numero_de_exibição.length > 0){
            txtTitle4.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_3_numero_de_exibição[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_4_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_4_numero_de_exibição.length > 0){
            txtTitle5.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_4_numero_de_exibição[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_5_numero_de_exibição != null && obj.page.custom_fields.como_fazemos_5_numero_de_exibição.length > 0){
            txtTitle6.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_5_numero_de_exibição[0]));
        }

        /************** Instruções *******************/

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_0_texto != null && obj.page.custom_fields.como_fazemos_0_texto.length > 0){
            txtPasso1.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_0_texto[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_1_texto != null && obj.page.custom_fields.como_fazemos_1_texto.length > 0){
            txtPasso2.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_1_texto[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_2_texto != null && obj.page.custom_fields.como_fazemos_2_texto.length > 0){
            txtPasso3.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_2_texto[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_3_texto != null && obj.page.custom_fields.como_fazemos_3_texto.length > 0){
            txtPasso4.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_3_texto[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_4_texto != null && obj.page.custom_fields.como_fazemos_4_texto.length > 0){
            txtPasso5.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_4_texto[0]));
        }

        if(obj.page.custom_fields!= null && obj.page.custom_fields.como_fazemos_5_texto != null && obj.page.custom_fields.como_fazemos_5_texto.length > 0){
            txtPasso6.setText(Html.fromHtml(obj.page.custom_fields.como_fazemos_5_texto[0]));
        }

    }


}
