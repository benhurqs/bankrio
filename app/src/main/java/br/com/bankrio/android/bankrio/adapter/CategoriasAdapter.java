package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.CategoriaObj;

/**
 * Created by Benhur on 03/03/16.
 */
public class CategoriasAdapter extends BaseAdapter {
    private ArrayList<CategoriaObj> categorias;
    private LayoutInflater inflater;


    public CategoriasAdapter(Context context,  ArrayList<CategoriaObj> categorias) {
        this.categorias = categorias;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return categorias.size();
    }

    @Override
    public CategoriaObj getItem(int position) {
        return categorias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        CategoriaObj categoriaObj = getItem(position);

        view = inflater.inflate(R.layout.item_categoria,null);

        TextView txtTitle = (TextView)view.findViewById(R.id.txt_categoria_item);
        txtTitle.setText(Html.fromHtml(categoriaObj.title));

        return view;
    }
}