package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.RelatoriosPostsObj;

/**
 * Created by Benhur on 25/03/16.
 */
public class RelatoriosAdapter extends BaseAdapter {
    private List<RelatoriosPostsObj> relatorios;
    private LayoutInflater inflater;
    private Context context;


    public RelatoriosAdapter(Context context, List<RelatoriosPostsObj> relatorios) {
        this.relatorios = relatorios;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return relatorios.size();
    }

    @Override
    public RelatoriosPostsObj getItem(int position) {
        return relatorios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final RelatoriosPostsObj relatoriosPostsObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_relatorio,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(relatoriosPostsObj.custom_fields != null && relatoriosPostsObj.custom_fields.descrição_do_relatorio != null && relatoriosPostsObj.custom_fields.descrição_do_relatorio.length > 0) {
            TextView txtDesc = (TextView) view.findViewById(R.id.txt_desc);
            txtDesc.setText(Html.fromHtml(relatoriosPostsObj.custom_fields.descrição_do_relatorio[0]));
        }

        if(relatoriosPostsObj.title != null){
            TextView txtFundo = (TextView) view.findViewById(R.id.txt_nome);
            txtFundo.setText(Html.fromHtml(relatoriosPostsObj.title));
        }


        return view;
    }
}
