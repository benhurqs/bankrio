package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.FormulariosEvent;
import br.com.bankrio.android.bankrio.model.FormulariosModel;

/**
 * Created by Benhur on 25/03/16.
 */
public class FormulariosRest extends CoreComm {

    private FormulariosEvent event;

    public FormulariosRest(Context context) {
        super(context, RequestType.GET, R.string.formularios_url);
        event = new FormulariosEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        FormulariosModel obj = gson.fromJson(json.toString(), FormulariosModel.class);

        if(obj.posts.length > 0) {
            event.obj = obj;
            bus.post(event);

            CacheManager.saveFormularios(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
