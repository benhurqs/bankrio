package br.com.bankrio.android.bankrio.events;

/**
 * Created by Benhur on 03/04/16.
 */
public class ProgressEvent {
    public int progress;

    public String respost1;
    public String respost2;
    public String respost3;
    public String respost4;

    public String nome;
    public String telefone;
    public String nome_profissional;

    @Override
    public String toString() {
        return "ProgressEvent{" +
                " respost1='" + respost1 + '\'' +
                ", respost2='" + respost2 + '\'' +
                ", respost3='" + respost3 + '\'' +
                ", respost4='" + respost4 + '\'' +
                ", nome='" + nome + '\'' +
                ", telefone='" + telefone + '\'' +
                ", nome_profissional='" + nome_profissional + '\'' +
                '}';
    }
}
