package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.IntradayEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.DetailObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 11/06/16.
 */
public class IntradayRest extends WFCoreComm {

    public IntradayRest(Context context, String name) {
        super(context, RequestType.GET, context.getString(R.string.wf_intraday_url,name, Utils.getDate()));
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        DetailObj[] obj = gson.fromJson(json.toString(), DetailObj[].class);
        IntradayEvent event = new IntradayEvent();

        if(obj.length > 0) {
            event.event = obj;
            bus.post(event);
        }else{
            event.error = context.getResources().getString(R.string.no_found);
            bus.post(event);
        }
    }
}