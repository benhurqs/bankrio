package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.FormulariosPostsObj;

/**
 * Created by Benhur on 25/03/16.
 */
public class FormulariosAdapter extends BaseAdapter {
    private List<FormulariosPostsObj> formularios;
    private LayoutInflater inflater;
    private Context context;


    public FormulariosAdapter(Context context, List<FormulariosPostsObj> formularios) {
        this.formularios = formularios;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return formularios.size();
    }

    @Override
    public FormulariosPostsObj getItem(int position) {
        return formularios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final FormulariosPostsObj formulariosObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_relatorio,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(formulariosObj.custom_fields != null && formulariosObj.custom_fields.descricao_do_formulario != null && formulariosObj.custom_fields.descricao_do_formulario.length > 0) {
            TextView txtDesc = (TextView) view.findViewById(R.id.txt_desc);
            txtDesc.setText(Html.fromHtml(formulariosObj.custom_fields.descricao_do_formulario[0]));
        }

        if(formulariosObj.title != null){
            TextView txtFundo = (TextView) view.findViewById(R.id.txt_nome);
            txtFundo.setText(Html.fromHtml(formulariosObj.title));
        }


        return view;
    }
}
