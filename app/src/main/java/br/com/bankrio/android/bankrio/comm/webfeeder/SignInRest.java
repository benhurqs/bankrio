package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 25/05/16.
 */
public class SignInRest extends WFCoreComm {

    private WFsignListener listener;

    public SignInRest(Context context, WFsignListener listener) {
        super(context, RequestType.POST, context.getString(R.string.wf_sign_in_url));
        this.listener = listener;
    }

    public boolean enviar(boolean login) {
        if(login){
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void sucesssWF(boolean success) {
        if(listener != null) {
            listener.response(success);
        }
//        bus.post(json);

    }


    public interface WFsignListener{
        public void response(boolean success);
    }

}
