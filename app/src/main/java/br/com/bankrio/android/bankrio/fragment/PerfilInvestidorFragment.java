package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.ProgressEvent;
import br.com.bankrio.android.bankrio.fragment.Questions.Question1Fragment;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 03/04/16.
 */
public class PerfilInvestidorFragment extends SuperFragment {

    private View view;
    private EventBus bus;
    private TextView txtNum2, txtNum3, txtNum4, txtNum5;
    private ImageView imgNum2, imgNum3, imgNum4, imgNum5;
    private ProgressBar progressIndicator;
    private ProgressEvent obj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_perfil, container, false);
        initView();
        return view;
    }


    private void initView() {
        setTitle("Perfil Investidor");

        bus = EventBus.getDefault();
        obj = new ProgressEvent();

        txtNum2 = (TextView) view.findViewById(R.id.txt_num_2);
        txtNum3 = (TextView) view.findViewById(R.id.txt_num_3);
        txtNum4 = (TextView) view.findViewById(R.id.txt_num_4);
        txtNum5 = (TextView) view.findViewById(R.id.txt_num_5);

        imgNum2 = (ImageView) view.findViewById(R.id.img_num_2);
        imgNum3 = (ImageView) view.findViewById(R.id.img_num_3);
        imgNum4 = (ImageView) view.findViewById(R.id.img_num_4);
        imgNum5 = (ImageView) view.findViewById(R.id.img_num_5);

        progressIndicator = (ProgressBar) view.findViewById(R.id.progress_indicator);
        progressIndicator.setProgress(0);

        changeFragment(new Question1Fragment());

    }

    private void updateProgressIndicator(ProgressEvent event) {
        int position = event.progress;
        switch (position) {
            case 1:
                txtNum2.setTextColor(getResources().getColor(R.color.blue_ind));
                imgNum2.setBackgroundDrawable(getResources().getDrawable(R.drawable.perfil_ind_selected));
                progressIndicator.setProgress(25 * position);
                obj.respost1 = event.respost1;
                break;
            case 2:
                txtNum3.setTextColor(getResources().getColor(R.color.blue_ind));
                imgNum3.setBackgroundDrawable(getResources().getDrawable(R.drawable.perfil_ind_selected));
                progressIndicator.setProgress(25 * position);
                obj.respost2 = event.respost2;
                break;
            case 3:
                txtNum4.setTextColor(getResources().getColor(R.color.blue_ind));
                imgNum4.setBackgroundDrawable(getResources().getDrawable(R.drawable.perfil_ind_selected));
                progressIndicator.setProgress(25 * position);
                obj.respost3 = event.respost3;
                break;
            case 4:
                txtNum5.setTextColor(getResources().getColor(R.color.blue_ind));
                imgNum5.setBackgroundDrawable(getResources().getDrawable(R.drawable.perfil_ind_selected));
                progressIndicator.setProgress(25 * position);
                obj.respost4 = event.respost4;
                break;
            case 5:
                obj.nome_profissional = event.nome_profissional;
                obj.nome = event.nome;
                obj.telefone = event.telefone;
                sendEmail();
            default:
                break;
        }

    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame_questions, fragment)
                .addToBackStack(fragment.getClass().toString())
                .commit();
//                .addToBackStack("xyz")
//                .commitAllowingStateLoss();

    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    public void onEvent(ProgressEvent event) {
        updateProgressIndicator(event);
    }

    private void sendEmail() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setType("plain/text");
        sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"atendimento@bankrio.com.br"});
        sendIntent.putExtra(Intent.EXTRA_CC, new String[]{"marcusribeirooo@gmail.com"});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Um usuário acaba de responder o Perfil Investidor no Aplicativo Android");
        sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(formatEmail()));
        startActivity(sendIntent);

    }

    private String formatEmail(){
        String email = "<br>" +
               "Nome: "+ obj.nome + "<br>" +
                "Telefone: " + obj.telefone + "<br>" +
                "<br>" +
                "<b>Suas respostas para o questionário, foram: </b>" +
                "<br>" +
                "<br>" +
                "1) Qual seu objetivo ao investir?" + "<br>" +
                "<b>" + obj.respost1 + "</b>" + "<br>" +
                "<br>" +
                "2) Por quanto tempo pretende investir seus recursos financeiros?" + "<br>" +
                "<b>" + obj.respost2 + "</b>" + "<br>" +
                "<br>" +
                "3) Qual a disponibilidade financeira que tem para investir?" + "<br>" +
                "<b>" + obj.respost3 + "</b>" + "<br>" +
                "<br>" +
                "4) Com relação aos riscos existentes no tipo de investimento escolhido, como reagiria ao verificar que, após o período de 06 meses, o mesmo apresenta retorno negativo?" + "<br>" +
                "<b>" + obj.respost4 + "</b>" + "<br>" +
                "<br>" +
                "Nome do profissional indicado foi: " + "<b>" + obj.nome_profissional + "</b>" +  "<br>" +
                "<br>" +
                "<br>" +
                "<br>" +
                "-" +
                "<br>" +
                "Este e-mail foi enviado do aplicativo BankRio Android";

        return email;
    }


}
