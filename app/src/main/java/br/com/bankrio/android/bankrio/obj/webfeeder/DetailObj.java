package br.com.bankrio.android.bankrio.obj.webfeeder;

/**
 * Created by Benhur on 08/06/16.
 */
public class DetailObj {

    /**
    {
        "symbol": "petr4",
            "price": 8.93,
            "open": 8.8,
            "high": 8.97,
            "low": 8.8,
            "previous": 8.62,
            "quantityTrades": 3474,
            "volumeAmount": 5989200,
            "volumeFinancier": 53301472,
            "timeTrade": "Jun 8, 2016 10:00:00 AM"
    },
     **/

    public String symbol;
    public float price;
    public float high;
    public float low;
    public float open;
    public float previous;
    public float quantityTrades;
    public float volumeAmount;
    public float volumeFinancier;
    public String timeTrade;

    public float change(){
        return (price - previous)*100/price;
    }

}
