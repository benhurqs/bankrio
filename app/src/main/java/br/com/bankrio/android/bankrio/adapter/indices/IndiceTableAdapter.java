package br.com.bankrio.android.bankrio.adapter.indices;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;

/**
 * Created by Benhur on 25/05/16.
 */
public class IndiceTableAdapter extends BaseAdapter {
    private List<CambioObj> cambioList;
    private LayoutInflater inflater;
    private Context context;


    public IndiceTableAdapter(Context context, List<CambioObj> cambios) {
        this.cambioList = cambios;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return cambioList.size();
    }

    @Override
    public CambioObj getItem(int position) {
        return cambioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final CambioObj rendaPostObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_indice,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(rendaPostObj != null) {

            TextView txtSigla = (TextView) view.findViewById(R.id.txt_sigla);
            txtSigla.setText(getSymbol(rendaPostObj.symbol));


            TextView txtNome = (TextView) view.findViewById(R.id.txt_nome);
            txtNome.setText(getName(rendaPostObj.symbol));


            TextView txtVariacao = (TextView) view.findViewById(R.id.txt_variacao);
            txtVariacao.setText(Html.fromHtml(rendaPostObj.getVariacao()) + "%");

            TextView txtCotacao = (TextView) view.findViewById(R.id.txt_cotacao);
            txtCotacao.setText("R$" + Html.fromHtml(rendaPostObj.getCotacao()));

            ImageView imgVariação = (ImageView)view.findViewById(R.id.img_variacao);
            if(rendaPostObj.change < 0){
                imgVariação.setImageResource(R.drawable.ic_ind_down);
            }else{
                imgVariação.setImageResource(R.drawable.ic_ind_up);
            }
        }

        if(isClicked()){
            ((ImageView)view.findViewById(R.id.img_seta)).setVisibility(View.VISIBLE);
        }



        return view;
    }


    protected boolean isClicked(){
        return true;
    }

    protected String getName(String name){
        return null;
    }

    protected String getSymbol(String subName){
        return null;
    }


}