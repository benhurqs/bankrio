package br.com.bankrio.android.bankrio.gcm;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.bankrio.android.bankrio.fragment.BlogFragment;
import br.com.bankrio.android.bankrio.fragment.FormulariosFragment;
import br.com.bankrio.android.bankrio.fragment.RelatorioFragment;
import br.com.bankrio.android.bankrio.fragment.RendaFixaFragment;
import br.com.bankrio.android.bankrio.fragment.TopFundosFragment;
import br.com.bankrio.android.bankrio.fragment.indices.IndicesFragment;

/**
 * Created by Benhur on 11/06/16.
 */
public class TargetNotification {

    public static final String ACOES = "acoes";
    public static final String BLOG = "blog";
    public static final String BOLSAS = "bolsas";
    public static final String CAMBIO = "cambio";
    public static final String FORMULARIOS = "formularios";
    public static final String INDICES = "indices";
    public static final String RENDA_FIXA = "rendafixa";
    public static final String RELATORIOS = "relatorios";
    public static final String TOP_FUNDOS = "topfundos";

    public static Fragment getFragment(String type){
        if(type.equalsIgnoreCase(ACOES)){
            return getIndice(type);
        }else if(type.equalsIgnoreCase(BLOG)){
            return new BlogFragment();
        }else if(type.equalsIgnoreCase(BOLSAS)){
            return getIndice(type);
        }else if(type.equalsIgnoreCase(CAMBIO)){
            return getIndice(type);
        }else if(type.equalsIgnoreCase(FORMULARIOS)){
            return new FormulariosFragment();
        }else if(type.equalsIgnoreCase(INDICES)){
            return getIndice(type);
        }else if(type.equalsIgnoreCase(RENDA_FIXA)){
            return new RendaFixaFragment();
        }else if(type.equalsIgnoreCase(RELATORIOS)){
            return new RelatorioFragment();
        }else if(type.equalsIgnoreCase(TOP_FUNDOS)){
            return new TopFundosFragment();
        }

        return null;
    }

    private static Fragment getIndice(String type){
        IndicesFragment fragment = new IndicesFragment();
        Bundle bundle = new Bundle();

        if(type.equalsIgnoreCase(BOLSAS)) {
            bundle.putInt("position", 0);
        }else  if(type.equalsIgnoreCase(ACOES)) {
            bundle.putInt("position", 1);
        }else  if(type.equalsIgnoreCase(INDICES)) {
            bundle.putInt("position", 2);
        }else  if(type.equalsIgnoreCase(CAMBIO)) {
            bundle.putInt("position", 3);
        }

        fragment.setArguments(bundle);
        return fragment;
    }
}
