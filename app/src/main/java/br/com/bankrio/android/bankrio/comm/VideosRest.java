package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.VideosEvent;
import br.com.bankrio.android.bankrio.model.VideosModel;

/**
 * Created by Benhur on 27/02/16.
 */
public class VideosRest extends CoreComm {

    private VideosEvent videosEvent;

    public VideosRest(Context context) {
        super(context, RequestType.GET, R.string.videos_url);
        videosEvent = new VideosEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        VideosModel obj = gson.fromJson(json.toString(), VideosModel.class);

        if(obj.posts.length > 0) {
            videosEvent.videos = obj;
            bus.post(videosEvent);

            CacheManager.saveVideos(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}

