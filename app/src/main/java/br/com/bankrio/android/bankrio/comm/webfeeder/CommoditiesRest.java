package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.events.CommoditiesEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 14/06/16.
 */
public class CommoditiesRest extends WFCoreComm {

    public CommoditiesRest(Context context) {
        super(context, RequestType.GET, R.string.wf_commodities_url);
    }

    @Override
    public boolean enviar() {
        Cache commodities = CacheManager.getCommodities();
        if(commodities != null && !Utils.needSync(commodities.currentTime, JsonType.WF_REFRESH)) {
            sendResponse(commodities.json);
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        CambioObj[] obj = gson.fromJson(json.toString(), CambioObj[].class);

        CommoditiesEvent event = new CommoditiesEvent();

        if(obj.length > 0) {
            event.commodities = obj;
            CacheManager.saveCommodities(json.toString());
        }else{
            event.error = context.getResources().getString(R.string.no_found);
        }

        bus.post(event);
    }
}
