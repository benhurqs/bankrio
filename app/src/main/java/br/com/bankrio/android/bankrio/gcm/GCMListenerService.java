package br.com.bankrio.android.bankrio.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.HomeActivity;
import br.com.bankrio.android.bankrio.model.PushModel;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 05/05/16.
 */
public class GCMListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Utils.dLog(data.toString());

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        String texto = data.getString("texto");
        String target = data.getString("target");

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        if(!Utils.ehVazio(message)) {
            sendNotification(message);
        }else if(!Utils.ehVazio(texto)){
            sendNotification(texto, target);
        }else{
            sendNotification(data.toString(), null);
        }

    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {

        String target = null;
        String text = null;

        Gson gson = new Gson();
        PushModel pushObj = gson.fromJson(message.toString(), PushModel.class);
        if(pushObj != null && pushObj.posts.length > 0) {
            target = pushObj.posts[0].custom_fields.target_notificacao[0];
        }

        if(pushObj != null && pushObj.posts.length > 0) {
            text = pushObj.posts[0].custom_fields.conteudo_da_notificacao[0];
        }


        sendNotification(text,target);
    }

    private void sendNotification(String text,String target){
        Intent intent = new Intent(this, HomeActivity.class);
        if(target != null) {
            intent.putExtra("TargetNotification", target);
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("BankRio")
                .setContentText(text != null ? text : "text is null")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }
}