package br.com.bankrio.android.bankrio.fragment.simuladores;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.comm.SimuladorRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.SimuladorEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.SimuladorObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 23/04/16.
 */
public class FaculdadeFragment extends SuperFragment {

    private View view;

    private EditText edtTempo, edtCusto;
    private Button btnCalcular;
    private TextView txtValor;
    private LinearLayout llResutado;
    private ProgressBar progress;
    private EventBus bus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_faculdade, container, false);
        initView();
        return view;
    }

    private void initView(){
        setTitle("Simuladores");

        bus = EventBus.getDefault();

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        edtTempo = (EditText)view.findViewById(R.id.edt_tempo);
        edtCusto = (EditText)view.findViewById(R.id.edt_custo);

        llResutado = (LinearLayout)view.findViewById(R.id.ll_resultado);
        llResutado.setVisibility(View.GONE);

        txtValor = (TextView)view.findViewById(R.id.txt_result);

        btnCalcular = (Button)view.findViewById(R.id.btn_calcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPISimuladores();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    private void callAPISimuladores(){
        if(Utils.ehVazio(edtCusto.getText().toString()) ||
                Utils.ehVazio(edtTempo.getText().toString())){
            Utils.showAlertDialog(getActivity(), getString(R.string.required));
            return;
        }

        SimuladorObj obj = new SimuladorObj();
        obj.custo = Integer.parseInt(edtCusto.getText().toString());
        obj.tempo = Integer.parseInt(edtTempo.getText().toString());

        SimuladorRest rest = new SimuladorRest(this.getContext(),SimuladorRest.FACULDADE);
        if(rest.enviar(obj)){
            progress.setVisibility(View.VISIBLE);
            disableAllViews();
        }

    }

    private void disableAllViews(){
        btnCalcular.setClickable(false);
        edtCusto.setFocusable(false);
        edtTempo.setFocusable(false);
    }

    private void enabledAllViews(){
        btnCalcular.setClickable(true);
        edtCusto.setFocusableInTouchMode(true);
        edtTempo.setFocusableInTouchMode(true);

    }

    /**
     * Retorna o resultado
     * */
    public void onEvent(SimuladorEvent event){
        progress.setVisibility(View.GONE);
        showresult(event.valor);
        enabledAllViews();
    }

    private void showresult(String valor){
        llResutado.setVisibility(View.VISIBLE);
        txtValor.setText(valor);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        Utils.showAlertDialog(getActivity(), error.mensage);
        progress.setVisibility(View.GONE);
        enabledAllViews();
    }


}