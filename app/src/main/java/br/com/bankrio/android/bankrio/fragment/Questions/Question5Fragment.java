package br.com.bankrio.android.bankrio.fragment.Questions;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Random;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.comm.ProfissionalRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.ProfissionaisEvent;
import br.com.bankrio.android.bankrio.events.ProgressEvent;
import br.com.bankrio.android.bankrio.fragment.HomeFragment;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.ProfissionaisPostsObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 03/04/16.
 */
public class Question5Fragment extends SuperFragment {

    private View view;
    private EventBus bus;
    private ImageView imgPhoto;
    private TextView txtNome, txtCargo, txtDesc;
    private View loading;
    private Button btnEnviar;
    private EditText edtNome, edtTelefone;
    private LinearLayout llContent;
    private String nomeProfissional;
    private boolean finishRequest = false, finishTime = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_perfil_questao_5, container, false);
        initView();
        return view;
    }

    private void initView() {
        bus = EventBus.getDefault();

        imgPhoto = (ImageView) view.findViewById(R.id.img_photo);
        txtNome = (TextView) view.findViewById(R.id.txt_nome);
        txtCargo = (TextView) view.findViewById(R.id.txt_cargo);
        txtDesc = (TextView) view.findViewById(R.id.txt_descricao);

        edtNome = (EditText) view.findViewById(R.id.edt_nome);
        edtTelefone = (EditText) view.findViewById(R.id.edt_telefone);

        btnEnviar = (Button) view.findViewById(R.id.btn_enviar);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validate()){
                    Utils.showAlertDialog(getActivity(), getResources().getString(R.string.required));
                    return;
                }

                sendProfisional();
            }
        });

        llContent = (LinearLayout) view.findViewById(R.id.ll_formulario);
        loading = (View) view.findViewById(R.id.loading);

        final ImageView myView = (ImageView) view.findViewById(R.id.loading_gif);
        final TextView txtLoading = (TextView) view.findViewById(R.id.txt_loading);
        txtLoading.setVisibility(View.VISIBLE);
        ((Animatable) myView.getDrawable()).start();

        callAPIProfissionais();
        showLoading();

    }

    private void sendProfisional(){
        bus = EventBus.getDefault();
        ProgressEvent event = new ProgressEvent();
        event.progress = 5;
        event.nome = edtNome.getText().toString();
        event.telefone = edtTelefone.getText().toString();
        event.nome_profissional = nomeProfissional;
        bus.post(event);

        changeFragment(new HomeFragment());

    }

    private boolean validate(){
        return !Utils.ehVazio(edtNome.getText().toString()) && !Utils.ehVazio(edtTelefone.getText().toString());

    }

    private void callAPIProfissionais() {
        ProfissionalRest rest = new ProfissionalRest(getActivity());
        if (rest.enviar()) {
            loading.setVisibility(View.VISIBLE);
        }else{
            finishRequest = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de blogs
     */
    public void onEvent(ProfissionaisEvent event) {
        finishRequest = true;
        if (finishTime) {
            loading.setVisibility(View.GONE);
            llContent.setVisibility(View.VISIBLE);
        }

        Random r = new Random();
        int position = (r.nextInt(event.profissioniais.posts.length-1));

        ProfissionaisPostsObj profissional = event.profissioniais.posts[position];
        populateProfissional(profissional);

    }

    public void onEvent(ErrorEvent erro) {
        finishRequest = true;
        if (finishTime) {
            loading.setVisibility(View.GONE);
            llContent.setVisibility(View.VISIBLE);
        }
    }

    private void populateProfissional(ProfissionaisPostsObj profissional) {
        if (profissional.custom_fields != null && profissional.custom_fields.cargo_profissional != null && profissional.custom_fields.cargo_profissional.length > 0) {
            txtCargo.setText(Html.fromHtml(profissional.custom_fields.cargo_profissional[0].toString()));
        }

        if (profissional.custom_fields != null && profissional.custom_fields.curriculo_profissional != null && profissional.custom_fields.curriculo_profissional.length > 0) {
            txtDesc.setText(Html.fromHtml(profissional.custom_fields.curriculo_profissional[0].toString()));
        }

        if (profissional.title != null) {
            txtNome.setText(Html.fromHtml(profissional.title.toString()));
            nomeProfissional = txtNome.getText().toString();
        }

        if (profissional.attachments != null && profissional.attachments.length > 0
                && profissional.attachments[0].images != null
                && profissional.attachments[0].images.full != null) {

            Glide.with(view.getContext())
                    .load(profissional.attachments[0].images.full.url)
                    .override(profissional.attachments[0].images.full.width, profissional.attachments[0].images.full.height)
                    .into(imgPhoto);
        } else {
            Glide.with(view.getContext())
                    .load(R.drawable.placeholder)
                    .into(imgPhoto);
        }
    }

    private void showLoading() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                finishTime = true;
                if (finishRequest) {
                    loading.setVisibility(View.GONE);
                    llContent.setVisibility(View.VISIBLE);
                }

            }
        }, 3000);
    }
}

