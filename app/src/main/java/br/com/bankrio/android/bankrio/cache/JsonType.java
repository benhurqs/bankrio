package br.com.bankrio.android.bankrio.cache;

/**
 * Created by Benhur on 20/03/16.
 */
public class JsonType {

    public static final int BLOG = 1;
    public static final int VIDEO = 2;
    public static final int CATEGORIAS = 3;
    public static final int RENDAFIXA = 4;
    public static final int RELATORIO = 5;
    public static final int FORMULARIO = 6;
    public static final int TOP_FUNDOS = 7;
    public static final int PROFISSIONAIS = 8;
    public static final int SOBRE = 9;
    public static final int ESCRITORIOS = 10;
    public static final int WF_BOLSAS = 11;
    public static final int WF_ACOES = 12;
    public static final int WF_INDICES = 13;
    public static final int WF_CAMBIO = 14;
    public static final int WF_SIGNIN = 15;
    public static final int WF_HISTORICO = 16;
    public static final int WF_INTRADAY = 17;
    public static final int WF_COMMODITIES = 18;
    public static final int WF_INDICADORES = 19;
    public static final int WF_INFLACAO = 20;
    public static final int WF_MARQUEE = 21;



    public static final int BLOG_REFRESH = 5;
    public static final int VIDEO_REFRESH = 5;
    public static final int CATEGORIA_REFRESH = 5;
    public static final int RENDAFIXA_REFRESH = 5;
    public static final int RELATORIO_REFRESH = 5;
    public static final int FORMULARIOS_REFRESH = 5;
    public static final int TOP_FUNDOS_REFRESH = 5;
    public static final int PROFISSIONAIS_REFRESH = 5;
    public static final int SOBRE_REFRESH = 5;
    public static final int ESCRITORIOS_REFRESH = 5;
    public static final int WF_REFRESH = 15;
}
