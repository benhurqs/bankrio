package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Modifier;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.BlogEvent;
import br.com.bankrio.android.bankrio.model.BlogModel;

/**
 * Created by Benhur on 03/03/16.
 */
public class BlogRest extends CoreComm {

    private BlogEvent event;
    private Cache blogCache;

    public BlogRest(Context context) {
        super(context, RequestType.GET, R.string.blog_url);
        event = new BlogEvent();
        blogCache = CacheManager.getBlog();

    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();
        BlogModel obj = gson.fromJson(json.toString(), BlogModel.class);

        if(obj.posts != null && obj.posts.length > 0) {
            event.blogs = obj;
            bus.post(event);
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }

        if(json != null) {
            CacheManager.saveBlog(json.toString());
        }
    }


}
