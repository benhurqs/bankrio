package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.events.AcoesEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 08/06/16.
 */
public class AcoesRest extends WFCoreComm {

    public AcoesRest(Context context) {
        super(context, RequestType.GET, R.string.wf_acoes_url);
    }

    @Override
    public boolean enviar() {
        Cache acoes = CacheManager.getAcoes();
        if(acoes != null && !Utils.needSync(acoes.currentTime, JsonType.WF_REFRESH)) {
            sendResponse(acoes.json);
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        CambioObj[] obj = gson.fromJson(json.toString(), CambioObj[].class);


        if(obj.length > 0) {
            AcoesEvent event = new AcoesEvent();
            event.acoes = obj;
            bus.post(event);
            CacheManager.saveAcoes(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
