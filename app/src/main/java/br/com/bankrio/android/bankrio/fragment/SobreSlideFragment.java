package br.com.bankrio.android.bankrio.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 27/02/16.
 */
public class SobreSlideFragment extends Fragment {

    public int position;
    private ImageView imgSlide;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.item_slide_sobre, container, false);

        managerImage(rootView);
        return rootView;
    }

    public void managerImage(View view){
        imgSlide = (ImageView)view.findViewById(R.id.img_slide);

        if(position == 0){
            imgSlide.setBackgroundResource(R.drawable.sobre_rio_de_janeiro);
        }else if(position == 1){
            imgSlide.setBackgroundResource(R.drawable.sobre_sao_paulo);
        }else{
            imgSlide.setBackgroundResource(R.drawable.sobre_roraima);
        }
    }
}

