package br.com.bankrio.android.bankrio.adapter.indices;

import android.content.Context;
import android.text.Html;

import java.util.List;

import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.MoedasNameUtils;

/**
 * Created by Benhur on 08/06/16.
 */
public class CambioAdapter extends IndiceTableAdapter {

    public CambioAdapter(Context context, List<CambioObj> cambios) {
        super(context, cambios);
    }

    @Override
    protected boolean isClicked() {
        return false;
    }

    @Override
    protected String getName(String name) {
        return "";
    }

    @Override
    protected String getSymbol(String symbol) {
        return Html.fromHtml(MoedasNameUtils.getName(symbol)).toString();
    }
}
