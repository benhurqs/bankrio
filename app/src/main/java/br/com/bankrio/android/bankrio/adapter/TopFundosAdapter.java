package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.TopFundosPostsObj;

/**
 * Created by Benhur on 25/03/16.
 */
public class TopFundosAdapter extends BaseAdapter {
    private List<TopFundosPostsObj> fundos;
    private LayoutInflater inflater;
    private Context context;


    public TopFundosAdapter(Context context, List<TopFundosPostsObj> fundos) {
        this.fundos = fundos;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return fundos.size();
    }

    @Override
    public TopFundosPostsObj getItem(int position) {
        return fundos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final TopFundosPostsObj topFundosPostsObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_fundos,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        /************************** Nome do Fundo *********************************/

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.title != null ) {
            TextView txtTitle = (TextView) view.findViewById(R.id.txt_nome_fundo);
            txtTitle.setText(Html.fromHtml(topFundosPostsObj.title));

        }

        /************************** Top Fundos 12 Meses *********************************/

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.valor_do_fundo_12 != null && topFundosPostsObj.custom_fields.valor_do_fundo_12.length > 0) {
            TextView txtValue12 = (TextView) view.findViewById(R.id.txt_value_12);
            txtValue12.setText(Html.fromHtml(topFundosPostsObj.custom_fields.valor_do_fundo_12[0])+ "%");

            ImageView imgInd12 = (ImageView) view.findViewById(R.id.img_ind_12);
            if(Float.parseFloat(topFundosPostsObj.custom_fields.valor_do_fundo_12[0]) > 0){
                imgInd12.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_up));
            }else{
                imgInd12.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_down));
            }
        }

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.cdi_12 != null && topFundosPostsObj.custom_fields.cdi_12.length > 0) {
            TextView txtCDI12 = (TextView) view.findViewById(R.id.txt_percent_12);
            txtCDI12.setText("(" + Html.fromHtml(topFundosPostsObj.custom_fields.cdi_12[0]) + "%)");
        }

        /************************** Top Fundos 24 Meses *********************************/

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.valor_do_fundo_24 != null && topFundosPostsObj.custom_fields.valor_do_fundo_24.length > 0) {
            TextView txtValue24 = (TextView) view.findViewById(R.id.txt_value_24);
            txtValue24.setText(Html.fromHtml(topFundosPostsObj.custom_fields.valor_do_fundo_24[0])+ "%");

            ImageView imgInd24 = (ImageView) view.findViewById(R.id.img_ind_24);
            if(Float.parseFloat(topFundosPostsObj.custom_fields.valor_do_fundo_24[0]) > 0){
                imgInd24.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_up));
            }else{
                imgInd24.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_down));
            }
        }

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.cdi_24 != null && topFundosPostsObj.custom_fields.cdi_24.length > 0) {
            TextView txtCDI24 = (TextView) view.findViewById(R.id.txt_percent_24);
            txtCDI24.setText("(" + Html.fromHtml(topFundosPostsObj.custom_fields.cdi_24[0]) + "%)");
        }

        /************************** Top Fundos 36 Meses *********************************/

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.valor_do_fundo_36 != null && topFundosPostsObj.custom_fields.valor_do_fundo_36.length > 0) {
            TextView txtValue36 = (TextView) view.findViewById(R.id.txt_value_36);
            txtValue36.setText(Html.fromHtml(topFundosPostsObj.custom_fields.valor_do_fundo_36[0])+ "%");

            ImageView imgInd36 = (ImageView) view.findViewById(R.id.img_ind_36);
            if(Float.parseFloat(topFundosPostsObj.custom_fields.valor_do_fundo_36[0]) > 0){
                imgInd36.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_up));
            }else{
                imgInd36.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ind_down));
            }
        }

        if(topFundosPostsObj.custom_fields != null && topFundosPostsObj.custom_fields.cdi_36 != null && topFundosPostsObj.custom_fields.cdi_36.length > 0) {
            TextView txtCDI36 = (TextView) view.findViewById(R.id.txt_percent_36);
            txtCDI36.setText("(" + Html.fromHtml(topFundosPostsObj.custom_fields.cdi_36[0]) + "%)");
        }



        return view;
    }
}
