package br.com.bankrio.android.bankrio.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.BlogActivity;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 03/03/16.
 */
public class BlogAdapter extends BaseAdapter {
    private List<BlogPostObj> blogs;
    private LayoutInflater inflater;
    private Context context;


    public BlogAdapter(Context context, List<BlogPostObj> blogs) {
        this.blogs = blogs;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return blogs.size();
    }

    @Override
    public BlogPostObj getItem(int position) {
        return blogs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position,View v, ViewGroup parent) {
        final BlogPostObj blogPostObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_blog,null);

        if(blogPostObj.categories != null && blogPostObj.categories.length > 0) {
            TextView txtCategoria = (TextView) view.findViewById(R.id.txt_blog_categorie);
            txtCategoria.setText(Html.fromHtml(blogPostObj.categories[0].title));
        }

        if(blogPostObj.title != null){
            TextView txtDescription = (TextView) view.findViewById(R.id.txt_blog_title);
            txtDescription.setText(Html.fromHtml(blogPostObj.title));
        }

        if(blogPostObj.custom_fields != null
                && blogPostObj.custom_fields.conteudo_do_post != null
                && blogPostObj.custom_fields.conteudo_do_post.length > 0){
            TextView txtDescription = (TextView) view.findViewById(R.id.txt_description_blog);
            txtDescription.setText(Html.fromHtml(blogPostObj.custom_fields.conteudo_do_post[0]));
        }

        ImageView thumb = (ImageView)view.findViewById(R.id.img_blog_thumb);

        if(blogPostObj.attachments != null && blogPostObj.attachments.length > 0
                && blogPostObj.attachments[0].images != null
                && blogPostObj.attachments[0].images.post_thumbnail != null) {

            Glide.with(view.getContext())
                    .load(blogPostObj.attachments[0].images.post_thumbnail.url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(thumb);
        }else{
            Glide.with(view.getContext())
                    .load(R.drawable.placeholder)
                    .into(thumb);
        }

        RelativeLayout rlLeiaMais = (RelativeLayout) view.findViewById(R.id.rl_leia_mais);
        rlLeiaMais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent blog = new Intent(context, BlogActivity.class);
                blog.putExtra(Utils.BLOG_OBJECT, blogPostObj);
                context.startActivity(blog);
            }
        });

        return view;
    }
}
