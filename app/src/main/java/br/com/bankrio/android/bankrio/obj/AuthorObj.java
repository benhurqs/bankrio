package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

/**
 * Created by Benhur on 27/02/16.
 */
public class AuthorObj implements Serializable {

    public int id;
    public String slug;
    public String name;
    public String first_name;
    public String last_name;
    public String nickname;
    public String url;
    public String description;
}
