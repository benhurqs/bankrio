package br.com.bankrio.android.bankrio.fragment.indices;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.CommoditiesEvent;
import br.com.bankrio.android.bankrio.events.IndicadoresEvent;
import br.com.bankrio.android.bankrio.events.InflacaoEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.MoedasNameUtils;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 17/06/16.
 */
public class IndiceTableFragment extends SuperFragment {

    private View view;
    private EventBus bus;
    private ProgressBar progressC, progressInd, progressInf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indice_indices, container, false);
        initView();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de commodities
     * */
    public void onEvent(CommoditiesEvent event){
        progressC.setVisibility(View.GONE);
        if(Utils.ehVazio(event.error)){
            populateCommodities(event.commodities);
        }
    }

    /**
     * Retorna um array de Inflacao
     * */
    public void onEvent(InflacaoEvent event){
        progressInf.setVisibility(View.GONE);
        if(Utils.ehVazio(event.error)){
            populateInflacao(event.inflacao);
        }
    }

    /**
     * Retorna um array de Indicadores
     * */
    public void onEvent(IndicadoresEvent event){
        progressInd.setVisibility(View.GONE);
        if(Utils.ehVazio(event.error)){
            populateIndicadores(event.indicadores);
        }
    }



    private void initView(){
        bus = EventBus.getDefault();
        progressC = (ProgressBar)view.findViewById(R.id.progress_commodities);
        progressInd = (ProgressBar)view.findViewById(R.id.progress_indicadores);
        progressInf = (ProgressBar)view.findViewById(R.id.progress_inflacao);


        callAPI();
    }

    private void callAPI(){
        if(WFRest.sendRest(getActivity(), JsonType.WF_INDICES)){
            progressC.setVisibility(View.VISIBLE);
            progressInd.setVisibility(View.VISIBLE);
            progressInf.setVisibility(View.VISIBLE);
        }
    }

    private void populateCommodities(CambioObj[] commodities){
        LinearLayout commoditiesRoot = (LinearLayout)view.findViewById(R.id.ll_commodities);
        for (int position = 0; position < commodities.length; position++) {
            View to_add = getActivity().getLayoutInflater().inflate(R.layout.item_indice,
                    commoditiesRoot,false);

            LinearLayout llItem = (LinearLayout) to_add.findViewById(R.id.ll_item);
            if(position % 2 == 1){
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_item));
            }else{
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            }

            final CambioObj rendaPostObj = commodities[position];
            if(rendaPostObj != null) {

                TextView txtSigla = (TextView) to_add.findViewById(R.id.txt_sigla);
                txtSigla.setText(Html.fromHtml(rendaPostObj.symbol).toString());


                TextView txtNome = (TextView) to_add.findViewById(R.id.txt_nome);
                txtNome.setText(Html.fromHtml(MoedasNameUtils.getName(rendaPostObj.symbol)).toString());


                TextView txtVariacao = (TextView) to_add.findViewById(R.id.txt_variacao);
                txtVariacao.setText(Html.fromHtml(rendaPostObj.getVariacao()) + "%");

                TextView txtCompra = (TextView) to_add.findViewById(R.id.txt_cotacao);
                txtCompra.setText("R$" + Html.fromHtml(String.valueOf(rendaPostObj.ask).replace(".",",")));

                ImageView imgVariação = (ImageView)to_add.findViewById(R.id.img_variacao);
                if(rendaPostObj.change < 0){
                    imgVariação.setImageResource(R.drawable.ic_ind_down);
                }else{
                    imgVariação.setImageResource(R.drawable.ic_ind_up);
                }
            }

            commoditiesRoot.addView(to_add);
        }
    }

    private void populateInflacao(CambioObj[] inflacao){
        LinearLayout commoditiesRoot = (LinearLayout)view.findViewById(R.id.ll_inflacao);
        for (int position = 0; position < inflacao.length; position++) {
            View to_add = getActivity().getLayoutInflater().inflate(R.layout.item_indice_inflacao,
                    commoditiesRoot,false);

            LinearLayout llItem = (LinearLayout) to_add.findViewById(R.id.ll_item);
            if(position % 2 == 1){
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_item));
            }else{
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            }

            final CambioObj rendaPostObj = inflacao[position];
            if(rendaPostObj != null) {

                TextView txtNome = (TextView) to_add.findViewById(R.id.txt_nome);
                txtNome.setText(Html.fromHtml(MoedasNameUtils.getName(rendaPostObj.symbol)).toString());


                TextView txtValor = (TextView) to_add.findViewById(R.id.txt_valor);
                txtValor.setText(Html.fromHtml(String.format("%.2f", rendaPostObj.changeMonth).replace(".",",")) + "%");

                TextView txtData = (TextView) to_add.findViewById(R.id.txt_data);
                txtData.setText(Utils.convertDateTimeToDate(rendaPostObj.timeUpdate));

                ImageView imgVariação = (ImageView)to_add.findViewById(R.id.img_variacao);
                if(rendaPostObj.change < 0){
                    imgVariação.setImageResource(R.drawable.ic_ind_down);
                }else{
                    imgVariação.setImageResource(R.drawable.ic_ind_up);
                }
            }

            commoditiesRoot.addView(to_add);
        }
    }

    private void populateIndicadores(CambioObj[] indicadores){
        LinearLayout commoditiesRoot = (LinearLayout)view.findViewById(R.id.ll_indicadores);
        for (int position = 0; position < indicadores.length; position++) {
            View to_add = getActivity().getLayoutInflater().inflate(R.layout.item_indice_inflacao,
                    commoditiesRoot,false);

            LinearLayout llItem = (LinearLayout) to_add.findViewById(R.id.ll_item);
            if(position % 2 == 1){
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_item));
            }else{
                llItem.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            }

            final CambioObj rendaPostObj = indicadores[position];
            if(rendaPostObj != null) {

                TextView txtNome = (TextView) to_add.findViewById(R.id.txt_nome);
                txtNome.setText(Html.fromHtml(MoedasNameUtils.getName(rendaPostObj.symbol)).toString());


                TextView txtValor = (TextView) to_add.findViewById(R.id.txt_valor);
                txtValor.setText(Html.fromHtml(String.format("%.2f", rendaPostObj.changeMonth).replace(".",",")) + "%");

                TextView txtData = (TextView) to_add.findViewById(R.id.txt_data);
                txtData.setText(Utils.convertDateTimeToDate(rendaPostObj.timeUpdate));

                ImageView imgVariação = (ImageView)to_add.findViewById(R.id.img_variacao);
                if(rendaPostObj.change < 0){
                    imgVariação.setImageResource(R.drawable.ic_ind_down);
                }else{
                    imgVariação.setImageResource(R.drawable.ic_ind_up);
                }
            }

            commoditiesRoot.addView(to_add);
        }
    }
}
