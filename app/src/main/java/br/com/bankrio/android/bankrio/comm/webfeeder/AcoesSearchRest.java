package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.events.AcoesEvent;
import br.com.bankrio.android.bankrio.events.AcoesSearchEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by benhurqs on 05/07/16.
 */
public class AcoesSearchRest extends WFCoreComm {

    public AcoesSearchRest(Context context, String sigla) {
        super(context, RequestType.GET, context.getString(R.string.wf_acoes_search_url, sigla));
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);
        sendResponse(json.toString());

    }


    private void sendResponse(String json) {
        Gson gson = new Gson();
        CambioObj obj = gson.fromJson(json.toString(), CambioObj.class);

        AcoesSearchEvent event = new AcoesSearchEvent();

        if (obj != null && obj.tickSize != 0) {
            event.acao = obj;
        } else {
            event.error = context.getResources().getString(R.string.no_found);
        }

        bus.post(event);
    }
}
