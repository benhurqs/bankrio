package br.com.bankrio.android.bankrio.fragment.indices;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.indices.IndiceSlideAdapter;
import br.com.bankrio.android.bankrio.events.IndicesEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 25/05/16.
 */
public class IndicesFragment extends SuperFragment {

    private View view;
    private EventBus bus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indices, container, false);
        initView();
        return view;
    }

    private void initView(){
        setTitle("Índices");

        bus = EventBus.getDefault();

        Bundle bundle = this.getArguments();
        int position = 0;
        if (bundle != null) {
            position = bundle.getInt("position", 0);
        }

        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) view.findViewById(R.id.viewpager);
        pager.setAdapter(new IndiceSlideAdapter(getActivity(),getActivity().getSupportFragmentManager(), this.getArguments()));
        pager.setCurrentItem(position);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                IndicesEvent event = new IndicesEvent();
                event.position = position;
                bus.post(event);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Custom tab bar
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        tabs.setTextColor(getActivity().getResources().getColor(R.color.white));
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);

    }


}
