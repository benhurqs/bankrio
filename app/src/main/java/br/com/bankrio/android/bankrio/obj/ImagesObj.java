package br.com.bankrio.android.bankrio.obj;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Benhur on 27/02/16.
 */
public class ImagesObj  implements Serializable {

    public ImagesSizeObj full;
    public ImagesSizeObj thumbnail;
    public ImagesSizeObj medium;

    @SerializedName("post-thumbnail")
    public ImagesSizeObj post_thumbnail;
}
