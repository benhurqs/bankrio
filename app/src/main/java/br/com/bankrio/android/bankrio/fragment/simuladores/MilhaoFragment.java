package br.com.bankrio.android.bankrio.fragment.simuladores;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.comm.SimuladorRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.SimuladorEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.SimuladorObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 23/04/16.
 */
public class MilhaoFragment extends SuperFragment {

    private View view;
    private EditText edtAplicacaoMensal, edtIdade, edtInvestimento;
    private Button btnCalcular;
    private TextView txtAnos, txtMeses;
    private LinearLayout llResutado;
    private ProgressBar progress;
    private EventBus bus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_milhao, container, false);
        initView();
        return view;
    }

    private void initView(){
        setTitle("Simuladores");
        bus = EventBus.getDefault();

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        edtAplicacaoMensal = (EditText)view.findViewById(R.id.edt_mensal);
        edtIdade = (EditText)view.findViewById(R.id.edt_idade);
        edtInvestimento = (EditText)view.findViewById(R.id.edt_invest);

        llResutado = (LinearLayout)view.findViewById(R.id.ll_resultado);
        llResutado.setVisibility(View.GONE);

        txtAnos = (TextView)view.findViewById(R.id.txt_anos);
        txtMeses = (TextView)view.findViewById(R.id.txt_meses);

        btnCalcular = (Button)view.findViewById(R.id.btn_calcular);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPISimuladores();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    private void callAPISimuladores(){
        if(Utils.ehVazio(edtAplicacaoMensal.getText().toString()) ||
                Utils.ehVazio(edtIdade.getText().toString()) ||
                Utils.ehVazio(edtInvestimento.getText().toString())){
            Utils.showAlertDialog(getActivity(), getString(R.string.required));
            return;
        }

        SimuladorObj obj = new SimuladorObj();
        obj.aplicacaoMensal = Integer.parseInt(edtAplicacaoMensal.getText().toString());
        obj.idade = Integer.parseInt(edtIdade.getText().toString());
        obj.investimentoAtual = Integer.parseInt(edtInvestimento.getText().toString());

        SimuladorRest rest = new SimuladorRest(this.getContext(),SimuladorRest.PRIMEIRO_MILHAO);
        if(rest.enviar(obj)){
            progress.setVisibility(View.VISIBLE);
            disableAllViews();
        }

    }

    private void disableAllViews(){
        btnCalcular.setClickable(false);
        edtInvestimento.setFocusable(false);
        edtIdade.setFocusable(false);
        edtAplicacaoMensal.setFocusable(false);
    }

    private void enabledAllViews(){
        btnCalcular.setClickable(true);
        edtInvestimento.setFocusableInTouchMode(true);
        edtAplicacaoMensal.setFocusableInTouchMode(true);
        edtIdade.setFocusableInTouchMode(true);

    }

    /**
     * Retorna um array de blogs
     * */
    public void onEvent(SimuladorEvent event){
        progress.setVisibility(View.GONE);
        showresult(event.anos, event.meses);
        enabledAllViews();
    }

    private void showresult(int anos, int meses){
        llResutado.setVisibility(View.VISIBLE);
        txtMeses.setText(String.valueOf(meses) + (meses == 1 ? " mês" : " meses"));
        txtAnos.setText(String.valueOf(anos) + (anos == 1 ? " ano" : " anos"));
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
        enabledAllViews();
    }


}