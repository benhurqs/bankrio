package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.adapter.FormulariosAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.FormulariosRest;
import br.com.bankrio.android.bankrio.events.FormulariosEvent;
import br.com.bankrio.android.bankrio.model.FormulariosModel;
import br.com.bankrio.android.bankrio.obj.FormulariosPostsObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 25/03/16.
 */
public class FormulariosFragment extends RelatorioFragment {


    private FormulariosAdapter formulariosAdapter;

    @Override
    protected void initView() {
        super.initView();
        setTitle("Formulários");
    }

    @Override
    public void refresh() {
        FormulariosRest rest = new FormulariosRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
            lvRelatorios.setVisibility(View.GONE);
        }
    }

    @Override
    protected void callAPI(){
        Cache formularios = CacheManager.getFormularios();
        if(formularios != null && !Utils.needSync(formularios.currentTime, JsonType.FORMULARIOS_REFRESH)){
            Gson gson = new Gson();
            FormulariosModel obj = gson.fromJson(formularios.json.toString(), FormulariosModel.class);

            FormulariosEvent event = new FormulariosEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.obj = obj;
                onEvent(event);
            }

            return;
        }

        refresh();
    }

    /**
     * Retorna um array de formularios
     * */
    public void onEvent(FormulariosEvent event){
        formulariosAdapter = new FormulariosAdapter(getActivity(), Arrays.asList(event.obj.posts));
        lvRelatorios.setAdapter(formulariosAdapter);
        progress.setVisibility(View.GONE);
        lvRelatorios.setVisibility(View.VISIBLE);
    }

    protected void onClickItem(int position){

        FormulariosPostsObj formulariosPostsObj = (FormulariosPostsObj)lvRelatorios.getItemAtPosition(position);

        if(formulariosPostsObj != null && formulariosPostsObj.attachments != null && formulariosPostsObj.attachments.length > 0 && !Utils.ehVazio(formulariosPostsObj.attachments[0].url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(formulariosPostsObj.attachments[0].url.toString().trim()));
            startActivity(browserIntent);
        }else {
            Toast.makeText(this.getActivity(), "Nenhum arquivo encontrado!", Toast.LENGTH_LONG).show();
        }
    }
}
