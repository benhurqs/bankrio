package br.com.bankrio.android.bankrio.obj.webfeeder;

/**
 * Created by benhurqs on 10/07/16.
 */
public class MarqueeObj {

    public String name;
    public float change;
    public float bid;
    public float open = -1;

    public String getVariacao(){
        return String.format("%.2f", change*100).replace(".",",");
    }

    public String getCotacao(){
        return String.format("%.2f", bid).replace(".",",");
    }

}
