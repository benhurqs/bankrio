package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

/**
 * Created by Benhur on 27/02/16.
 */
public class AttachmentsObj implements Serializable {
    public int id;
    public String url;
    public String slug;
    public String title;
    public String description;
    public String caption;
    public int parent;
    public String mime_type;
    public ImagesObj images;
}
