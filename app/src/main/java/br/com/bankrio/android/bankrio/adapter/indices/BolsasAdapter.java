package br.com.bankrio.android.bankrio.adapter.indices;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.webfeeder.BolsasObj;

/**
 * Created by Benhur on 04/06/16.
 */
public class BolsasAdapter extends BaseAdapter {
    private List<BolsasObj> bolsasList;
    private LayoutInflater inflater;
    private Context context;


    public BolsasAdapter(Context context, List<BolsasObj> bolsas) {
        this.bolsasList = bolsas;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return bolsasList.size();
    }

    @Override
    public BolsasObj getItem(int position) {
        return bolsasList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        final BolsasObj bolsaObj = getItem(position);

        final View view = inflater.inflate(R.layout.item_bolsa,null);

        LinearLayout llItem = (LinearLayout) view.findViewById(R.id.ll_item);
        if(position % 2 == 1){
            llItem.setBackgroundColor(context.getResources().getColor(R.color.bg_item));
        }else{
            llItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(bolsaObj != null) {

            TextView txtNome = (TextView) view.findViewById(R.id.txt_nome);
            txtNome.setText(Html.fromHtml(bolsaObj.getName(context)));

            TextView txtAtualizacao = (TextView) view.findViewById(R.id.txt_atualizacao);
            txtAtualizacao.setText(Html.fromHtml(bolsaObj.getLastUpdate()));

            TextView txtAbertura = (TextView) view.findViewById(R.id.txt_abertura);
            txtAbertura.setText(Html.fromHtml(String.valueOf(bolsaObj.open)));

            TextView txtMinDia = (TextView) view.findViewById(R.id.txt_min);
            txtMinDia.setText(Html.fromHtml(String.valueOf(bolsaObj.low)));

            TextView txtMaxDia = (TextView) view.findViewById(R.id.txt_max);
            txtMaxDia.setText(Html.fromHtml(String.valueOf(bolsaObj.high)));

            TextView txtVariacao = (TextView) view.findViewById(R.id.txt_variacao);
            txtVariacao.setText(Html.fromHtml(String.valueOf(bolsaObj.change)));


            TextView txtVarMensal = (TextView) view.findViewById(R.id.txt_var_mensal);
            txtVarMensal.setText(Html.fromHtml(String.valueOf(bolsaObj.changeMonth)));

            TextView txtPrecoMed = (TextView) view.findViewById(R.id.txt_preco_med);
            txtPrecoMed.setText(Html.fromHtml(String.valueOf(bolsaObj.average)));

            TextView txtQtdUlt = (TextView) view.findViewById(R.id.txt_qtd_ult_neg);
            txtQtdUlt.setText(Html.fromHtml(String.valueOf(bolsaObj.quantityLast)));

            TextView txtQtdAcu = (TextView) view.findViewById(R.id.txt_qtd_acum_neg);
            txtQtdAcu.setText(Html.fromHtml(String.valueOf(bolsaObj.quantityTrades)));

            TextView txtVolAcu = (TextView) view.findViewById(R.id.txt_vol_acum);
            txtVolAcu.setText(Html.fromHtml(String.valueOf(bolsaObj.volumeAmount)));

            TextView txtVolFin = (TextView) view.findViewById(R.id.txt_vol_fin);
            txtVolFin.setText(Html.fromHtml(String.valueOf(bolsaObj.volumeFinancier)));

            TextView txtFecMen = (TextView) view.findViewById(R.id.txt_fec_mensal);
            txtFecMen.setText(Html.fromHtml(String.valueOf(bolsaObj.lastTradeLastMonth)));

            TextView txtFecSemanal = (TextView) view.findViewById(R.id.txt_fec_sem);
            txtFecSemanal.setText(Html.fromHtml(String.valueOf(bolsaObj.lastTradeLastWeek)));

            TextView txtFecAnual = (TextView) view.findViewById(R.id.txt_fec_anual);
            txtFecAnual.setText(Html.fromHtml(String.valueOf(bolsaObj.lastTradeLastYear)));

        }



        return view;
    }


}