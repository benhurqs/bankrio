package br.com.bankrio.android.bankrio.comm;

/**
 * Created by Benhur on 20/12/15.
 */

import android.content.Context;
import android.os.Looper;
import android.support.annotation.StringRes;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.BuildConfig;
import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.utils.Utils;
import cz.msebera.android.httpclient.Header;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 20/11/15.
 */
public class CoreComm  {

    private AsyncHttpClient client;
    protected RequestType type;
    protected String url;
    protected Context context;

    protected EventBus bus;
    protected ErrorEvent error;

    protected RequestParams params;
    protected AsyncHttpResponseHandler responseHandler = null;

    private static SyncHttpClient syncHttpClient = null;
    private static AsyncHttpClient asyncHttpClient = null;



    public enum RequestType{
        GET, POST
    }

    //Contrutor
    public CoreComm(Context context, RequestType type, @StringRes int url){
        this(context,type,context.getString(url));
    }

    public CoreComm(Context context, RequestType type, String url){
        this.context = context;
        this.type = type;
        this.url = url;


        this.bus = EventBus.getDefault();
        this.error = new ErrorEvent();

        if(syncHttpClient == null){
            Utils.dLog("New sync");
            syncHttpClient = new SyncHttpClient();
        }

        if(asyncHttpClient == null){
            Utils.dLog("New async");
            asyncHttpClient = new AsyncHttpClient();
        }
    }

    /**
     * Envia a requisição para o servidor,
     * verificando antes a conexão e retornando
     * um boolean com o resultado do envio.
     */
    public boolean enviar(){
        if(Utils.isDeviceOnline(context)){
            startComm();
            return true;
        }else{
//            error(context.getResources().getString(R.string.no_connection));
            error.mensage = context.getResources().getString(R.string.no_connection);
            bus.post(error);
        }

        return false;
    }

    /**
     * Inicializa os calbacks das requisições
     * tratando os casos de falhas pré definidos na API
     */
    protected void startComm(){
        responseHandler = new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                Utils.dLog("response sucess " +  statusCode + " ==> " + response);
                jsonArray(response);
            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    Utils.dLog("response sucess" +  statusCode + " ==> " + response.toString());
                }catch(Exception ex){
                    Utils.dLog("ErroRestClient" + ex.getMessage());
                }
                jsonObject(response);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Utils.dLog("response falha 1 " + statusCode + " ==> " + responseString + " - " + throwable);
                error.mensage = context.getResources().getString(R.string.error_connection);
                bus.post(error);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Utils.dLog("response falha 2 " + statusCode + " ==> " + errorResponse);
//                error(context.getResources().getString(R.string.error_connection));
                error.mensage = context.getResources().getString(R.string.error_connection);
                bus.post(error);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Utils.dLog("response falha 3 " + statusCode + " ==> " + errorResponse);
//                error(context.getResources().getString(R.string.error_connection));
                error.mensage = context.getResources().getString(R.string.error_connection);
                bus.post(error);

            }

        };

        setParams();
        switch (type){
            case POST:
                post();
                break;
            case GET:
                get();
                break;
            default:
                break;
        }
    }

    public void setParams(){}


    protected void get() {
        Utils.dLog("URL ===> " + getBaseurl() + url);
        getClient().get(getBaseurl() + url, params, responseHandler);
    }

    protected void post() {
        if(params != null) {
            Utils.dLog(params.toString());
        }
        Utils.dLog("URL ===> " + getBaseurl() + url);
        getClient().post(getBaseurl() + url, params, responseHandler);
    }


    /**
     * Retorna o client que será usado para as requisições
     * e decide, caso necessário, usar um client sincrono,
     * mas usa como default o assincrono.
     */
    private AsyncHttpClient getClient()
    {
        if (Looper.myLooper() == null)
            return configureClient(syncHttpClient);
        return configureClient(asyncHttpClient);
    }

    /**
     * Configuração do client, passando os valores
     * de Versao do app no
     * header do request, assim como a configuracao
     * do timeout
     */
    private AsyncHttpClient configureClient(AsyncHttpClient client){

        client.setUserAgent("android mobile app-android");
        try{
            String versao = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            client.addHeader("versao", versao);
            Utils.dLog("Versao =====>" + versao);
        }catch (Exception e){
            Utils.dLog("Error" + e.getMessage());
        }

        client.setTimeout(20000);

        return client;
    }

//    protected void error(String error){}
    protected void jsonArray(JSONArray json){}
    protected void jsonObject(JSONObject json){}

    protected String getBaseurl(){
        return BuildConfig.API_URL;
    }

}