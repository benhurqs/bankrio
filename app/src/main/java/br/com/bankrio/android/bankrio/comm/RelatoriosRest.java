package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.RelatorioEvent;
import br.com.bankrio.android.bankrio.model.RelatoriosModel;

/**
 * Created by Benhur on 25/03/16.
 */
public class RelatoriosRest extends CoreComm {

    private RelatorioEvent event;

    public RelatoriosRest(Context context) {
        super(context, RequestType.GET, R.string.relatorio_url);
        event = new RelatorioEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        RelatoriosModel obj = gson.fromJson(json.toString(), RelatoriosModel.class);

        if(obj.posts.length > 0) {
            event.obj = obj;
            bus.post(event);

            CacheManager.saveRelatorios(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
