package br.com.bankrio.android.bankrio.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONObject;

import java.util.ArrayList;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.FaleConoscoRest;
import br.com.bankrio.android.bankrio.databinding.FrameEscritorioBinding;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.EscritorioObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 27/02/16.
 */
public class FaleConoscoFragment extends SuperFragment {

    private View view;
    private LinearLayout llEscritorios;
    private EventBus bus;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_fale_conosco, container, false);
        initView();
        return view;
    }

    private void initView(){
        setTitle("Fale Conosco");

        bus = EventBus.getDefault();
        progress = (ProgressBar)view.findViewById(R.id.progress);
        llEscritorios = (LinearLayout)view.findViewById(R.id.ll_escritorios);

        callAPI();

    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de escritorios
     * */
    public void onEvent(ArrayList<EscritorioObj> event){
        Utils.dLog("escritorios - " + event.size());
        progress.setVisibility(View.GONE);
        populateEscritotios(event);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }


    private void callAPI(){
        FaleConoscoRest rest = new FaleConoscoRest(this.getContext());
        Cache escritorios = CacheManager.getEscritorios();
        if(escritorios != null && !Utils.needSync(escritorios.currentTime, JsonType.ESCRITORIOS_REFRESH)){
            progress.setVisibility(View.GONE);
            try {
                rest.parseEscritorios(new JSONObject(escritorios.json.toString()));
                populateEscritotios(rest.escritorioObjs);

            }catch (Exception e){
                Utils.dLog(e.getMessage());
            }
        }else{
            if(rest.enviar()){
                progress.setVisibility(View.VISIBLE);
            }
        }
    }

    public void populateEscritotios(ArrayList<EscritorioObj> escritorioObjs){
        llEscritorios.removeAllViews();

        for( EscritorioObj obj : escritorioObjs){
            LayoutInflater inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            FrameEscritorioBinding binding = DataBindingUtil.inflate(inflater, R.layout.frame_escritorio, null, false);
            binding.setEscritorio(obj);
            binding.viewSeparetor.setBackgroundColor(getActivity().getResources().getColor(obj.getColor()));
//            binding.txtEndereco.setText(Html.fromHtml(obj.address));

            llEscritorios.addView(binding.getRoot());
        }
    }
}
