package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.FormulariosEvent;
import br.com.bankrio.android.bankrio.obj.EscritorioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 04/05/16.
 */
public class FaleConoscoRest extends CoreComm {

    private FormulariosEvent event;
    public ArrayList<EscritorioObj> escritorioObjs;

    public FaleConoscoRest(Context context) {
        super(context, RequestType.GET, R.string.fale_conosco_url);
        event = new FormulariosEvent();
        escritorioObjs = new ArrayList<EscritorioObj>();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        parseEscritorios(json);

        if(escritorioObjs.size() > 0){
            bus.post(escritorioObjs);

            CacheManager.saveEscritorios(json.toString());
        }

    }

    public void parseEscritorios(JSONObject json){
        try{
            JSONObject pageJson = json.getJSONObject("page");
            if(pageJson == null){
                return;
            }

            JSONObject custom_fields = pageJson.getJSONObject("custom_fields");
            if(custom_fields == null){
                return;
            }

            JSONArray escritorios = custom_fields.getJSONArray("escritorios");
            if(escritorios  == null || escritorios.length() <= 0){
                return;
            }

            String numEscritorios = escritorios.getString(0);
            int num = Integer.parseInt(numEscritorios);

            for(int i = 0 ; i < num ; i++ ){
                JSONArray nome_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_nome_escritorio");
                JSONArray endereço_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_endereço_escritorio");
                JSONArray link_google_maps_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_link_google_maps_escritorio");
                JSONArray tipo_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_tipo_escritorio");
                JSONArray telefone_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_telefone_escritorio");
                JSONArray adress_escritorio = custom_fields.getJSONArray("escritorios_" + i + "_adress_escritorio");

                Utils.dLog(nome_escritorio.getString(0) + " - " +
                        endereço_escritorio.getString(0)+ " - " +
                        link_google_maps_escritorio.getString(0)+ " - " +
                        tipo_escritorio.getString(0)+ " - " +
                        telefone_escritorio.getString(0)+ " - " +
                        adress_escritorio.getString(0)

                );

                EscritorioObj obj = new EscritorioObj();
                obj.id = i + 1;
                obj.nome = nome_escritorio.getString(0);
                obj.endereco = endereço_escritorio.getString(0);
                obj.google_maps = link_google_maps_escritorio.getString(0);
                obj.tipo = tipo_escritorio.getString(0);
                obj.telefone = telefone_escritorio.getString(0);
                obj.address = adress_escritorio.getString(0);

                escritorioObjs.add(obj);

            }

        }catch (Exception e){
            Utils.dLog(e.getMessage().toString());
        }
    }
}
