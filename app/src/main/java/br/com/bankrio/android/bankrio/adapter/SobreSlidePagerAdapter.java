package br.com.bankrio.android.bankrio.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.bankrio.android.bankrio.fragment.SobreSlideFragment;

/**
 * Created by Benhur on 27/02/16.
 */
public class SobreSlidePagerAdapter extends FragmentStatePagerAdapter {

    /**
     * The number of pages (wizard steps) to show.
     */
    public static final int NUM_PAGES = 3;
    public static final int SLIDE_TIME = 4 * 1000;

    public SobreSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        SobreSlideFragment sobreSlideFragment = new SobreSlideFragment();
        sobreSlideFragment.position = position;
        return sobreSlideFragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
