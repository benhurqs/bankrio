package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 04/05/16.
 */
public class EscritorioObj implements Serializable {

    public int id;
    public String nome;
    public String endereco;
    public String google_maps;
    public String tipo;
    public String telefone;
    public String address;

    public int getColor(){
        if(id % 2 == 0){
            return R.color.esc_blue;
        }else if(id % 3 == 0){
            return R.color.esc_yellow;
        }else{
            return R.color.esc_blue_light;
        }
    }

    public String addressFormated(){
        return address.replace("</br>","\n");
    }
}
