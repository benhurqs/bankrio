package br.com.bankrio.android.bankrio.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import br.com.bankrio.android.bankrio.BankRioApplication;
import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.fragment.HomeFragment;
import br.com.bankrio.android.bankrio.fragment.indices.IndicesFragment;
import br.com.bankrio.android.bankrio.fragment.indices.detail.HistoricoFragment;
import br.com.bankrio.android.bankrio.fragment.indices.detail.IndiceDetailFragment;
import br.com.bankrio.android.bankrio.gcm.GCMPreference;
import br.com.bankrio.android.bankrio.gcm.RegistrationIntentService;
import br.com.bankrio.android.bankrio.helpers.MenuDrawerManager;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

public class HomeActivity extends MenuDrawerManager {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private BankRioApplication app;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bus = EventBus.getDefault();
        app = (BankRioApplication)getApplication();

        if(!deepLinkNotification()) {
            changeFragment(new HomeFragment());
        }
        initDrawerMenu();
        registrationGCM();

    }

    private boolean deepLinkNotification(){
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (extras.containsKey("TargetNotification")) {
                String msg = extras.getString("TargetNotification");

                HomeFragment home = new HomeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("TargetNotification", msg);
                home.setArguments(bundle);

                changeFragment(home);

                Utils.dLog(" push ===>  " + msg);
                return true;
            }
        }

        return false;

    }



    private void callWFSignIn(){
//        SignInRest rest = new SignInRest(this);
//        rest.enviar(app.wf_login);
    }

    private void registrationGCM(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(GCMPreference.SENT_TOKEN_TO_SERVER, false);
                String token = sharedPreferences
                        .getString(GCMPreference.TOKEN, null);
                if (sentToken) {
                    Utils.dLog("Token enviado");
                    Utils.dLog("Token  - " + token);
                } else {
                    Utils.dLog("Erro ao enviar o token");
                }
            }
        };

        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * Retorna um array de blogs
     * */
    public void onEvent(String signIn){
        BankRioApplication app = (BankRioApplication)getApplication();
        app.setWfLoginResult(signIn.equals("true"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        callWFSignIn();
        bus.register(this);
        registerReceiver();
    }

    @Override
    protected void onPause() {
        bus.unregister(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(GCMPreference.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Utils.dLog( "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void onClickItens(View v){
        getCurrentFragment().onClickItens(v);
    }

    @Override
    public void onBackPressed() {
        SuperFragment fragment = getCurrentFragment();
        if(fragment == null){
            return;
        }

        if(fragment.getClass().equals(HomeFragment.class)){
            HomeActivity.this.finish();
        }else if(fragment.getClass().equals(HistoricoFragment.class) || fragment.getClass().equals(IndiceDetailFragment.class) ){
            changeFragment(new IndicesFragment());
        }else{
            changeFragment(new HomeFragment());
        }

    }




}
