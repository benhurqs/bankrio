package br.com.bankrio.android.bankrio.adapter.indices;

import android.content.Context;
import android.text.Html;

import java.util.List;

import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.MoedasNameUtils;

/**
 * Created by Benhur on 11/06/16.
 */
public class AcoesAdapter extends IndiceTableAdapter {

    public AcoesAdapter(Context context, List<CambioObj> cambios) {
        super(context, cambios);
    }

    protected boolean isClicked(){
        return true;
    }

    @Override
    protected String getName(String name) {
        return Html.fromHtml(MoedasNameUtils.getName(name)).toString();
    }

    @Override
    protected String getSymbol(String symbol) {
        return Html.fromHtml(symbol).toString();
    }
}
