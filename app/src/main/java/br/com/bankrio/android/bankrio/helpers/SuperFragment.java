package br.com.bankrio.android.bankrio.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 21/02/16.
 */
public class SuperFragment extends Fragment {

    protected String title = "";

    public void onClickItens(View v){};

    public void changeFragment(Fragment fragment){
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();

        fragmentTransaction2.hide(this);
        fragmentTransaction2.replace(R.id.content_frame, fragment);
        fragmentTransaction2.addToBackStack("xyz");
        fragmentTransaction2.commit();
    }

    protected void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){

        Log.e("aqui","title ===> " + title);
        return title;
    }
}
