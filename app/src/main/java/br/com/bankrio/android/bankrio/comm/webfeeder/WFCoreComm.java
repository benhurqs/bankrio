package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;
import android.support.annotation.StringRes;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.BuildConfig;
import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.comm.CoreComm;
import br.com.bankrio.android.bankrio.utils.Utils;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Benhur on 25/05/16.
 */
public class WFCoreComm extends CoreComm {


    public WFCoreComm(Context context, RequestType type, @StringRes int url) {
        super(context, type, url);
    }

    public WFCoreComm(Context context, RequestType type, String url) {
        super(context, type, url);
    }

    /**
     * Inicializa os calbacks das requisições
     * tratando os casos de falhas pré definidos na API
     */
    @Override
    protected void startComm(){
        responseHandler = new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                Utils.dLog("response sucess " +  statusCode + " ==> " + response);
                jsonArray(response);
            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    Utils.dLog("response sucess" +  statusCode + " ==> " + response.toString());
                }catch(Exception ex){
                    Utils.dLog("ErroRestClient" + ex.getMessage());
                }
                jsonObject(response);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Utils.dLog("response falha 1 " + statusCode + " ==> " + responseString + " - " + throwable);
                if(type == RequestType.POST && responseString.equals("true")){
                    Utils.dLog("Success WebFeeder");
                    sucesssWF(true);
                    return;
                }

//                error.mensage = context.getResources().getString(R.string.error_connection);
//                bus.post(error);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Utils.dLog("response falha 2 " + statusCode + " ==> " + errorResponse);
//                error(context.getResources().getString(R.string.error_connection));
                error.mensage = context.getResources().getString(R.string.error_connection);
                bus.post(error);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Utils.dLog("response falha 3 " + statusCode + " ==> " + errorResponse);
//                error(context.getResources().getString(R.string.error_connection));
                error.mensage = context.getResources().getString(R.string.error_connection);
                bus.post(error);

            }

        };

        setParams();
        switch (type){
            case POST:
                post();
                break;
            case GET:
                get();
                break;
            default:
                break;
        }
    }

    @Override
    protected String getBaseurl() {
        return BuildConfig.WEB_FEEDER_URL;
    }
    protected void sucesssWF(boolean success){}
}
