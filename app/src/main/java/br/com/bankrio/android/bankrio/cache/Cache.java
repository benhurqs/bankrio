package br.com.bankrio.android.bankrio.cache;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by Benhur on 20/03/16.
 */

@Table(name = "Cache")
public class Cache extends Model {

    @Column(name = "json")
    public String json;

    @Column(name = "time")
    public Long currentTime;


    @Column(name = "type")
    public int type;


    @Override
    public String toString() {
        return "Cache{" +
                "json='" + json + '\'' +
                ", currentTime=" + currentTime +
                ", type=" + type +
                '}';
    }
}
