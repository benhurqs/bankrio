package br.com.bankrio.android.bankrio.adapter.indiceDetail;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.Arrays;
import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.fragment.indices.detail.HistoricoFragment;
import br.com.bankrio.android.bankrio.fragment.indices.detail.IntradayFragment;

/**
 * Created by Benhur on 31/05/16.
 */
public class IndiceDetailSlideAdapter extends FragmentStatePagerAdapter {

    private List<String> titles = null;
    private Bundle bundle = null;

    public IndiceDetailSlideAdapter(Context ctx, FragmentManager fm, Bundle bundle) {
        super(fm);
        titles = Arrays.asList(ctx.getResources().getStringArray(R.array.indice_detail_tabs));
        this.bundle = bundle;
    }

    public Fragment setBundleToFragment(Fragment fragment){
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return setBundleToFragment(new HistoricoFragment());
            default:
                return setBundleToFragment(new IntradayFragment());
        }
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position).toString();
    }
}

