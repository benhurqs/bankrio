package br.com.bankrio.android.bankrio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubePlayerView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.helpers.SuperActivity;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 12/03/16.
 */
public class BlogActivity extends SuperActivity {

    private BlogPostObj blogPostObj;
    private RelativeLayout youtubeVideo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        blogPostObj = (BlogPostObj)getIntent().getSerializableExtra(Utils.BLOG_OBJECT);
        if(blogPostObj != null) {
            initView();
        }
    }

    private void initView(){
        youtubeVideo = (RelativeLayout)findViewById(R.id.rl_youtube);
        youtubeVideo.setVisibility(View.GONE);

        if(blogPostObj.categories != null && blogPostObj.categories.length > 0) {
            TextView txtCategoria = (TextView) findViewById(R.id.txt_blog_categorie);
            txtCategoria.setText(Html.fromHtml(blogPostObj.categories[0].title));
        }

        if(blogPostObj.title != null){
            TextView txtDescription = (TextView) findViewById(R.id.txt_blog_title);
            txtDescription.setText(Html.fromHtml(blogPostObj.title));
        }

        if(blogPostObj.custom_fields != null
                && blogPostObj.custom_fields.conteudo_do_post != null
                && blogPostObj.custom_fields.conteudo_do_post.length > 0){
            TextView txtDescription = (TextView) findViewById(R.id.txt_description_blog);
            txtDescription.setText(Html.fromHtml(getEmbededVideo(blogPostObj.custom_fields.conteudo_do_post[0]).replace("\r\n","<br>")));
        }

        ImageView thumb = (ImageView)findViewById(R.id.img_blog_thumb);

        if(blogPostObj.attachments != null && blogPostObj.attachments.length > 0
                && blogPostObj.attachments[0].images != null
                && blogPostObj.attachments[0].images.full != null) {

            Glide.with(this)
                    .load(blogPostObj.attachments[0].images.full.url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(thumb);
        }else{
            Glide.with(this)
                    .load(R.drawable.placeholder)
                    .into(thumb);
        }

    }

    public void onClickBack(View view){
        this.finish();
    }

    private String getEmbededVideo(String json){
        json = json.replace("[embed]","XXX").replace("[/embed]","XXX");
        String[] explode = json.split("XXX");
        if(explode.length > 1){
            Utils.dLog("link ===> " + explode[1]);
            showVideo(explode[1]);
        }

        return explode[0];
    }

    private void showVideo(final String link){
        if(Utils.ehVazio(link)){
           return;
        }


        final String[] youtubeId = link.split("youtu.be/");
        youtubeVideo.setVisibility(View.VISIBLE);

        ImageView thumb = (ImageView) findViewById(R.id.img_thumb);
        thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent youtube = new Intent(BlogActivity.this, YoutubePlayerActivity.class);
                youtube.putExtra(Utils.YoutuBE_OBJECT, youtubeId[1]);
                startActivity(youtube);
            }
        });
        if(youtubeId.length > 1){
            Glide.with(this)
                    .load("http://img.youtube.com/vi/"+ youtubeId[1] + "/0.jpg")
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(thumb);
        }else{
            Glide.with(this)
                    .load(R.drawable.placeholder)
                    .into(thumb);
        }
    }
}
