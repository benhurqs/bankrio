package br.com.bankrio.android.bankrio.fragment.indices;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.indices.AcoesAdapter;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.AcoesEvent;
import br.com.bankrio.android.bankrio.events.AcoesSearchEvent;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.fragment.indices.detail.IndiceDetailFragment;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 08/06/16.
 */
public class AcoesFragment extends SuperFragment {

    private View view;
    private ListView lvIndices;
    private EventBus bus;
    private AcoesAdapter indiceTableAdapter;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indice_acoes, container, false);
        initView();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de cambio
     * */
    public void onEvent(AcoesEvent event){
        indiceTableAdapter = new AcoesAdapter(getActivity(), Arrays.asList(event.acoes));
        lvIndices.setAdapter(indiceTableAdapter);
        progress.setVisibility(View.GONE);
    }

    public void onEvent(AcoesSearchEvent obj){
        ArrayList<CambioObj> acoes = new ArrayList<CambioObj>();
        if(Utils.ehVazio(obj.error)) {
            acoes.add(obj.acao);
            indiceTableAdapter = new AcoesAdapter(getActivity(), acoes);
            lvIndices.setAdapter(indiceTableAdapter);
        }else{
            progress.setVisibility(View.GONE);
            Utils.showAlertDialog(getActivity(), obj.error);
        }


    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        progress.setVisibility(View.GONE);
//        Utils.showAlertDialog(getActivity(), error.mensage);
//        Utils.dLog("error request - " + error.mensage);
    }

    private void initView(){
        bus = EventBus.getDefault();
        progress = (ProgressBar)view.findViewById(R.id.progress);

        lvIndices = (ListView)view.findViewById(R.id.lv_indices);
        lvIndices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CambioObj obj = (CambioObj) parent.getAdapter().getItem(position);
                if(obj != null) {
                    IndiceDetailFragment fragment = new IndiceDetailFragment();
                    Bundle arg = new Bundle();
                    arg.putString("name",obj.symbol);
                    fragment.setArguments(arg);
                    changeFragment(fragment);
                }
            }
        });

        callAPI();
    }

    private void callAPI(){
        if(WFRest.sendRest(getActivity(), JsonType.WF_ACOES)){
            progress.setVisibility(View.VISIBLE);
        }
    }
}
