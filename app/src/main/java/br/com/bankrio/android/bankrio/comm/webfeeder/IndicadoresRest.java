package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.events.IndicadoresEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 16/06/16.
 */
public class IndicadoresRest extends WFCoreComm {

    public IndicadoresRest(Context context) {
        super(context, RequestType.GET, R.string.wf_indicadores_url);
    }

    @Override
    public boolean enviar() {
        Cache indicadores = CacheManager.getIndicadores();
        if(indicadores != null && !Utils.needSync(indicadores.currentTime, JsonType.WF_REFRESH)) {
            sendResponse(indicadores.json);
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        CambioObj[] obj = gson.fromJson(json.toString(), CambioObj[].class);

        IndicadoresEvent event = new IndicadoresEvent();

        if(obj.length > 0) {
            event.indicadores = obj;
            CacheManager.saveIndicadores(json.toString());
        }else{
            event.error = context.getResources().getString(R.string.no_found);
        }

        bus.post(event);
    }
}
