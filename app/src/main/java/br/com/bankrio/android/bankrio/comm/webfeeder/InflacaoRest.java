package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.events.InflacaoEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 17/06/16.
 */
public class InflacaoRest  extends WFCoreComm {

    public InflacaoRest(Context context) {
        super(context, RequestType.GET, R.string.wf_inflacao_url);
    }

    @Override
    public boolean enviar() {
        Cache inflacao = CacheManager.getInflacao();
        if(inflacao != null && !Utils.needSync(inflacao.currentTime, JsonType.WF_REFRESH)) {
            sendResponse(inflacao.json);
            return false;
        }
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        CambioObj[] obj = gson.fromJson(json.toString(), CambioObj[].class);

        InflacaoEvent event = new InflacaoEvent();

        if(obj.length > 0) {
            event.inflacao = obj;
            CacheManager.saveInflacao(json.toString());
        }else{
            event.error = context.getResources().getString(R.string.no_found);
        }

        bus.post(event);
    }
}
