package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.ProfissionaisEvent;
import br.com.bankrio.android.bankrio.model.ProfissionaisModel;

/**
 * Created by Benhur on 07/04/16.
 */
public class ProfissionalRest extends CoreComm {

    private ProfissionaisEvent event;

    public ProfissionalRest(Context context) {
        super(context, RequestType.GET, R.string.profissional_url);
        event = new ProfissionaisEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        ProfissionaisModel obj = gson.fromJson(json.toString(), ProfissionaisModel.class);

        if(obj.posts.length > 0) {
            event.profissioniais = obj;
            bus.post(event);

            CacheManager.saveProfissionais(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}