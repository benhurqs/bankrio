package br.com.bankrio.android.bankrio.fragment.Questions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.ProgressEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 03/04/16.
 */
public class Question2Fragment extends SuperFragment {

    private View view;

    private LinearLayout llResp1, llResp2, llResp3, llResp4, llResp5, llResp6;
    private ImageView imgResp1, imgResp2, imgResp3, imgResp4, imgResp5, imgResp6;
    private EventBus bus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_perfil_questao_2, container, false);
        initView();
        return view;
    }

    private void initView(){
        imgResp1 = (ImageView)view.findViewById(R.id.img_resp1);
        llResp1 = (LinearLayout)view.findViewById(R.id.ll_resp_1);
        llResp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp1, getString(R.string.q2r1));
            }
        });

        imgResp2 = (ImageView)view.findViewById(R.id.img_resp2);
        llResp2 = (LinearLayout)view.findViewById(R.id.ll_resp_2);
        llResp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp2, getString(R.string.q2r2));
            }
        });

        imgResp3 = (ImageView)view.findViewById(R.id.img_resp3);
        llResp3 = (LinearLayout)view.findViewById(R.id.ll_resp_3);
        llResp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp3, getString(R.string.q2r3));
            }
        });

        imgResp4 = (ImageView)view.findViewById(R.id.img_resp4);
        llResp4 = (LinearLayout)view.findViewById(R.id.ll_resp_4);
        llResp4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp4, getString(R.string.q2r4));
            }
        });

        imgResp5 = (ImageView)view.findViewById(R.id.img_resp5);
        llResp5 = (LinearLayout)view.findViewById(R.id.ll_resp_5);
        llResp5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp5, getString(R.string.q2r5));
            }
        });

        imgResp6 = (ImageView)view.findViewById(R.id.img_resp6);
        llResp6 = (LinearLayout)view.findViewById(R.id.ll_resp_6);
        llResp6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAnsewr(imgResp6, getString(R.string.q2r6));
            }
        });


    }

    private void selectAnsewr(ImageView img, String resp){
        img.setImageDrawable(getResources().getDrawable(R.drawable.btn_perfil_selected));

        bus = EventBus.getDefault();
        ProgressEvent event = new ProgressEvent();
        event.progress = 2;
        event.respost2 = resp;
        bus.post(event);

        changeFragment(new Question3Fragment());
    }

    public void changeFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

        ft.replace(R.id.content_frame_questions, fragment, "detailFragment");

        ft.commit();


    }


}
