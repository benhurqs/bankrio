package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.RendaFixaEvent;
import br.com.bankrio.android.bankrio.model.RendaFixaModel;

/**
 * Created by Benhur on 20/03/16.
 */
public class RendaFixaRest extends CoreComm {

    private RendaFixaEvent event;

    public RendaFixaRest(Context context) {
        super(context, RequestType.GET, R.string.renda_fixa_url);
        event = new RendaFixaEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        RendaFixaModel obj = gson.fromJson(json.toString(), RendaFixaModel.class);

        if(obj.posts.length > 0) {
            event.obj = obj;
            bus.post(event);

            CacheManager.saveRendaFixa(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}

