package br.com.bankrio.android.bankrio.obj;

/**
 * Created by Benhur on 23/04/16.
 */
public class SobreDataObj {

    public String[] texto_introducao;
    public String[] nossa_missao;
    public String[] nossos_valores;
    public String[] nossos_objetivos;
    public String[] como_fazemos;

    public String[] como_fazemos_0_numero_de_exibição;
    public String[] como_fazemos_0_pergunta;
    public String[] como_fazemos_0_texto;

    public String[] como_fazemos_1_numero_de_exibição;
    public String[] como_fazemos_1_pergunta;
    public String[] como_fazemos_1_texto;

    public String[] como_fazemos_2_numero_de_exibição;
    public String[] como_fazemos_2_pergunta;
    public String[] como_fazemos_2_texto;

    public String[] como_fazemos_3_numero_de_exibição;
    public String[] como_fazemos_3_pergunta;
    public String[] como_fazemos_3_texto;

    public String[] como_fazemos_4_numero_de_exibição;
    public String[] como_fazemos_4_pergunta;
    public String[] como_fazemos_4_texto;

    public String[] como_fazemos_5_numero_de_exibição;
    public String[] como_fazemos_5_pergunta;
    public String[] como_fazemos_5_texto;

    public String[] texto_comofazemos;
    public String[] galeria_bankrio_0_foto_da_galeria;
    public String[] galeria_bankrio_1_foto_da_galeria;
    public String[] galeria_bankrio;
    public String[] galeria_bankrio_2_foto_da_galeria;
    public String[] galeria_bankrio_3_foto_da_galeria;
    public String[] galeria_quem_somos_mobile_0_foto_galeria_mobile;
    public String[] galeria_quem_somos_mobile;
    public String[] galeria_quem_somos_mobile_1_foto_galeria_mobile;
    public String[] meta_description_seo;
    public String[] meta_keywords_seo;


}
