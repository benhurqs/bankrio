package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.HistoricoEvent;
import br.com.bankrio.android.bankrio.obj.webfeeder.DetailObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 25/05/16.
 */
public class HistoricoRest extends WFCoreComm {

    public HistoricoRest(Context context, String name) {
        super(context, RequestType.GET, context.getString(R.string.wf_historico_url,name, Utils.getIntervalDate()));
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
        Utils.dLog("Array ===> " + json.toString());

        sendResponse(json.toString());

    }

    private void sendResponse(String json){
        Gson gson = new Gson();
        DetailObj[] obj = gson.fromJson(json.toString(), DetailObj[].class);
        HistoricoEvent event = new HistoricoEvent();

        if(obj.length > 0) {
            event.event = obj;
            bus.post(event);
        }else{
            event.error = context.getResources().getString(R.string.no_found);
            bus.post(event);
        }
    }
}
