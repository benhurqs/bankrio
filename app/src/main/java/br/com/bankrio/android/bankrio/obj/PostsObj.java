package br.com.bankrio.android.bankrio.obj;

import java.io.Serializable;

/**
 * Created by Benhur on 03/03/16.
 */
public class PostsObj implements Serializable {

    public int id;
    public String type;
    public String slug;
    public String url;
    public String status;
    public String title;
    public String title_plain;
    public String content;
    public String excerpt;
    public String date;
    public String modified;
    public CategoriaObj[] categories;
    public TagsObj[] tags;
    public AuthorObj author;
    //        public CommentsObj comments;
    public AttachmentsObj[] attachments;
    public int comment_count;
    public String comment_status;


    public Boolean belongsToCategory(int id){
        boolean belongs = false;

        for(CategoriaObj category : categories){
            if(category.id == id){
                belongs = true;
                break;
            }
        }

        return belongs;

    }
}
