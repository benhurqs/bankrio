package br.com.bankrio.android.bankrio.gcm;

/**
 * Created by Benhur on 05/05/16.
 */
public class GCMPreference {
    public static final String TOKEN = "tokenId";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
