package br.com.bankrio.android.bankrio;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by Benhur on 20/03/16.
 */
public class BankRioApplication extends Application {

    public boolean wf_login = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }

    public void setWfLoginResult(boolean result){
        this.wf_login = result;
    }

}
