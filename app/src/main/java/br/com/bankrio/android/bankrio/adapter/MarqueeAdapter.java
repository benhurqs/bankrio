package br.com.bankrio.android.bankrio.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.obj.webfeeder.MarqueeObj;

/**
 * Created by Benhur on 19/06/16.
 */
public class MarqueeAdapter extends RecyclerView.Adapter<MarqueeAdapter.ViewHolder> {
    private MarqueeObj[] mDataset;
    private int quantItem = 10;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name, value, pontos;
        public ImageView imgInd;

        public ViewHolder(View v) {
            super(v);

            name = (TextView)v.findViewById(R.id.txt_name);
            value = (TextView)v.findViewById(R.id.txt_value);
            pontos = (TextView)v.findViewById(R.id.txt_pontos);
            imgInd = (ImageView) v.findViewById(R.id.img_ind);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MarqueeAdapter(MarqueeObj[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MarqueeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_marquee, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        int realPosition = position%(mDataset.length);

        MarqueeObj obj = mDataset[realPosition];
        holder.name.setText(obj.name);


        holder.value.setText(Html.fromHtml(obj.getVariacao()) + "%");

        if(obj.change < 0){
            holder.imgInd.setImageResource(R.drawable.ic_ind_down);
            holder.value.setTextColor(holder.value.getRootView().getContext().getResources().getColor(R.color.red));
        }else{
            holder.imgInd.setImageResource(R.drawable.ic_ind_up);
            holder.value.setTextColor(holder.value.getRootView().getContext().getResources().getColor(R.color.green));
        }

        if(realPosition == 0){
            holder.pontos.setText(obj.open + " pts");
        }else{
            holder.pontos.setText("R$ " + Html.fromHtml(obj.getCotacao()));
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length*quantItem;
    }
}


