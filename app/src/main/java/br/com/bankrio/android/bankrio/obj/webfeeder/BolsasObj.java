package br.com.bankrio.android.bankrio.obj.webfeeder;

import android.content.Context;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 07/06/16.
 */
public class BolsasObj {

/****
 *
 * "symbol": "ibov",
 "timeUpdate": "07-06-2016 00:55:06",
 "dateTrade": "06-06-2016 00:00:00",
 "lastTrade": 50431.8,
 "previous": 50619.496,
 "change": -0.37079453,
 "changeMonth": 4.0437818,
 "bid": 0,
 "ask": 0,
 "timeLastTrade": "06-06-2016 17:21:00",
 "dateTradeObj": "Jun 6, 2016 12:00:00 AM",
 "quantity": 0,
 "quantityLast": 500,
 "quantityTrades": 624948,
 "volumeAmount": 313453408,
 "volumeFinancier": 4022801410,
 "high": 50923.543,
 "low": 50097.27,
 "open": 50627.242,
 "volumeBid": 0,
 "volumeAsk": 0,
 "volumeBetterBid": 0,
 "volumeBetterAsk": 0,
 "lastTradeLastWeek": 50619.496,
 "lastTradeLastMonth": 48471.71,
 "lastTradeLastYear": 43349.96,
 "interest": 0,
 "situation": "0",
 "average": 50497.598,
 "execPrice": 0,
 "tickSize": 2,
 "timeLastTradeSting": "172100",
 "dateLastTradeString": "20160606"
 *
 * **/

    public String symbol;
    public String timeUpdate;
    public String dateTrade;
    public float lastTrade;
    public float previous;
    public float change;
    public float changeMonth;
    public float bid;
    public float ask;
    public String timeLastTrade;
    public String dateTradeObj;
    public float quantity;
    public float quantityLast;
    public float quantityTrades;
    public float volumeAmount;
    public float volumeFinancier;
    public float high;
    public float low;
    public float open;
    public float volumeBid;
    public float volumeAsk;
    public float volumeBetterBid;
    public float volumeBetterAsk;
    public float lastTradeLastWeek;
    public float lastTradeLastMonth;
    public float lastTradeLastYear;
    public float interest;
    public float situation;
    public float average;
    public float execPrice;
    public float tickSize;
    public float timeLastTradeSting;
    public float dateLastTradeString;


    public String getLastUpdate(){
        if(Utils.ehVazio(timeUpdate)){
            return "";
        }

        String[] dataTime = timeUpdate.split(" ");

        if(dataTime.length <= 0) {
          return timeUpdate;
        }

        String[] hms = dataTime[1].split(":");

        return " " +hms[0].concat(":").concat(hms[1]);
    }

    public String getName(Context ctx){
        if(symbol.equalsIgnoreCase("ibov")){
            return ctx.getString(R.string.bovespa);
        }else if(symbol.equalsIgnoreCase("itag")){
            return ctx.getString(R.string.itag);
        }else{
            return "";
        }
    }

}
