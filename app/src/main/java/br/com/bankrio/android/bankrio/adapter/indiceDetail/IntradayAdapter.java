package br.com.bankrio.android.bankrio.adapter.indiceDetail;

import android.content.Context;

import java.util.List;

import br.com.bankrio.android.bankrio.obj.webfeeder.DetailObj;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 11/06/16.
 */
public class IntradayAdapter extends IndiceDetailTableAdapter {

    public IntradayAdapter(Context context, List<DetailObj> cambios) {
        super(context, cambios);
    }

    @Override
    protected String getValue(String date) {
        return Utils.convertHour(date);
    }
}
