package br.com.bankrio.android.bankrio.fragment.indices.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.indiceDetail.IntradayAdapter;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.IntradayEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 08/06/16.
 */
public class IntradayFragment extends SuperFragment {

    private View view;
    private ListView lvIndices;
    private EventBus bus;
    private IntradayAdapter indiceTableAdapter;
    private String name;
    private ProgressBar progress;
    private TextView txtError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indice_detail_intraday, container, false);
        if(getArguments() != null) {
            name = getArguments().getString("name");
        }
        initView();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de cambio
     * */
    public void onEvent(IntradayEvent event){
        if(Utils.ehVazio(event.error) && event.event.length > 0) {
            indiceTableAdapter = new IntradayAdapter(getActivity(), Arrays.asList(event.event));
            lvIndices.setAdapter(indiceTableAdapter);
            txtError.setVisibility(View.GONE);
        }else{
            txtError.setVisibility(View.VISIBLE);
        }



        progress.setVisibility(View.GONE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        Utils.showAlertDialog(getActivity(), error.mensage);
//        Utils.dLog("error request - " + error.mensage);
//        progress.setVisibility(View.GONE);
    }

    private void initView(){
        bus = EventBus.getDefault();
        txtError = (TextView)view.findViewById(R.id.txt_error);
        txtError.setVisibility(View.GONE);
        progress = (ProgressBar)view.findViewById(R.id.progress);
        lvIndices = (ListView)view.findViewById(R.id.lv_indices);

        callAPI();
    }

    private void callAPI(){
        if(WFRest.sendRest(getActivity(), JsonType.WF_INTRADAY, name)){
            progress.setVisibility(View.VISIBLE);
        }
    }


}
