package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.SimuladorEvent;
import br.com.bankrio.android.bankrio.obj.SimuladorObj;

/**
 * Created by Benhur on 30/04/16.
 */
public class SimuladorRest extends CoreComm {

    public static final int PRIMEIRO_MILHAO = 1;
    public static final int FACULDADE = 2;
    public static final int APOSENTADORIA = 3;

    private int simulador;
    private SimuladorObj respostas;

    public SimuladorRest(Context context, int simulador) {
        super(context, RequestType.POST, context.getString(R.string.simuladores_url) + simulador);
        this.simulador = simulador;
    }

    public boolean enviar(SimuladorObj respostas) {
        if(simulador > 3){
            ErrorEvent e = new ErrorEvent();
            e.mensage = "Simulador não encontrado";
            bus.post(e);
            return false;
        }

        this.respostas= respostas;
        setParams();
        return super.enviar();
    }

    @Override
    public void setParams() {
        params = new RequestParams();
        switch (simulador){
            case PRIMEIRO_MILHAO:
                setParams1();
                break;
            case FACULDADE:
                setParams2();
                break;
            case APOSENTADORIA:
                setPArams3();
                break;
        }

    }

    private void setParams1(){
        params.put("idade",respostas.idade);
        params.put("investimentoAtual",respostas.investimentoAtual);
        params.put("aplicacaoMensal",respostas.aplicacaoMensal);
    }

    private void setParams2(){
        params.put("tempo",respostas.tempo);
        params.put("custo",respostas.custo);

    }

    private void setPArams3(){
        setParams1();
        params.put("idadeAposentar",respostas.idadeAposentar);
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        SimuladorEvent obj = gson.fromJson(json.toString(), SimuladorEvent.class);

        if(obj != null) {
            bus.post(obj);
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}

