package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.SobreEvent;
import br.com.bankrio.android.bankrio.model.SobreModel;

/**
 * Created by Benhur on 23/04/16.
 */
public class SobreRest extends CoreComm {

    private SobreEvent event;

    public SobreRest(Context context) {
        super(context, RequestType.GET, R.string.sobre_url);
        event = new SobreEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);

    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        SobreModel obj = gson.fromJson(json.toString(), SobreModel.class);

        if(obj.page != null) {
            event.obj = obj;
            bus.post(event);

            CacheManager.saveSobre(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
