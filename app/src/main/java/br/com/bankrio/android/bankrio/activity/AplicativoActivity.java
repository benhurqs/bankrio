package br.com.bankrio.android.bankrio.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.R;

/**
 * Created by Benhur on 03/05/16.
 */
public class AplicativoActivity extends AppCompatActivity {

    private TextView txtDescricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aplicativo);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        initView();
    }

    private void initView(){
        txtDescricao = (TextView)this.findViewById(R.id.txt_app_desc);
        txtDescricao.setText(Html.fromHtml(descricao()));
    }

    private String descricao(){
        String txt = "<b>Nosso aplicativo tem como objetivo trazer as nossas informações e notícias como empresa, aqui você poderá ficar atualizado de todas as novidades no cenário financeiro." +
                "<br>" +
                "<br>" +
                "Fique por dentro do nosso blog e tire suas dúvidas com nosso atendimento em:" +
                "<br>" +
                "(21) 3852-4515 ou (11) 3818-0914." +
                "<br>" +
                "<br>" +
                "Nós "+ changeColorText("NÃO")  + " capturamos suas informações de geolocalização e nem seus dados multímida como fotos e vídeos." +
                "<br>" +
                "<br>" +
                "O aplicativo é 100% de caráter institucional e informativo.</b>" +
                "<br>";

        return txt;
    }

    private String changeColorText(String txt){
        int сolor = this.getResources().getColor(R.color.red);
        String сolorString = String.format("%X", сolor).substring(2); // !!strip alpha value!!
        return (String.format("<b><font color=\"#%s\">%s</font></b>", сolorString, txt));
    }

    public void onClickBack(View view){
        this.finish();
    }


}
