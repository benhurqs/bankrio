package br.com.bankrio.android.bankrio.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.VideosAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.VideosRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.VideosEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.VideosModel;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 27/02/16.
 */
public class VideosFragment extends SuperFragment {

    private View view;
    private VideosAdapter videosAdapter;
    private ListView lvVideos;
    private EventBus bus;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_videos, container, false);
        initView();
        return view;
    }


    private void initView() {
        setTitle("Videos");

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        lvVideos = (ListView)view.findViewById(R.id.lv_videos);
        bus = EventBus.getDefault();

        callVideoAPI();
    }

    private void callVideoAPI(){
        Cache videos = CacheManager.getVideos();
        if(videos != null  && !Utils.needSync(videos.currentTime, JsonType.VIDEO_REFRESH)){
            Gson gson = new Gson();
            VideosModel obj = gson.fromJson(videos.json.toString(), VideosModel.class);

            if(obj.posts.length > 0) {
                VideosEvent videosEvent = new VideosEvent();
                videosEvent.videos = obj;
                onEvent(videosEvent);

            }
            return;
        }


        VideosRest rest = new VideosRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Retorna um arry de videos
     * */
    public void onEvent(VideosEvent event){
        videosAdapter = new VideosAdapter(getActivity(),event.videos.posts);
        lvVideos.setAdapter(videosAdapter);
        progress.setVisibility(View.GONE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }
}