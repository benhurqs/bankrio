package br.com.bankrio.android.bankrio.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.bankrio.android.bankrio.fragment.VitrineSlideFragment;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;


/**
 * Created by Benhur on 19/02/16.
 */
public class VitrineSlidePagerAdapter extends FragmentStatePagerAdapter {


    /**
     * The number of pages (wizard steps) to show.
     */
    private static final int NUM_PAGES = 6;
    public BlogPostObj destaque;

    public VitrineSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public VitrineSlidePagerAdapter(FragmentManager fm, BlogPostObj destaque) {
        super(fm);
        this.destaque = destaque;
    }


    @Override
    public Fragment getItem(int position) {
        VitrineSlideFragment vitrinew  = new VitrineSlideFragment();
        vitrinew.destaque = destaque;
        vitrinew.position = position;
        return vitrinew;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

}
