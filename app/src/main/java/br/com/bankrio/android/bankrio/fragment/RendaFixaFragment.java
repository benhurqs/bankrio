package br.com.bankrio.android.bankrio.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.RendaFixaAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.RendaFixaRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.events.RendaFixaEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.RendaFixaModel;
import br.com.bankrio.android.bankrio.obj.RendaFixaPostsObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 20/03/16.
 */
public class RendaFixaFragment extends SuperFragment {

    private View view;
    private ListView lvRendaFixa;
    private RendaFixaAdapter rendaFixaAdapter;
    private EventBus bus;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_renda_fixa, container, false);
        initView();
        return view;
    }


    private void initView() {
        setTitle("Renda Fixa");

        bus = EventBus.getDefault();
        lvRendaFixa = (ListView)view.findViewById(R.id.lv_renda_fixa);
        lvRendaFixa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onClickItem(position);
            }
        });

        progress = (ProgressBar)view.findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        callRendaFixaAPI();

    }


    private void callRendaFixaAPI(){
        Cache blog = CacheManager.getRendaFixa();
        if(blog != null && !Utils.needSync(blog.currentTime, JsonType.RENDAFIXA_REFRESH)){
            Gson gson = new Gson();
            RendaFixaModel obj = gson.fromJson(blog.json.toString(), RendaFixaModel.class);

            RendaFixaEvent event = new RendaFixaEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.obj = obj;
                onEvent(event);
            }

            return;
        }

        RendaFixaRest rest = new RendaFixaRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
        }
    }


    /**
     * Retorna um array de blogs
     * */
    public void onEvent(RendaFixaEvent event){
        rendaFixaAdapter = new RendaFixaAdapter(getActivity(), Arrays.asList(event.obj.posts));
        lvRendaFixa.setAdapter(rendaFixaAdapter);
        progress.setVisibility(View.GONE);
        lvRendaFixa.setVisibility(View.VISIBLE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
//        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
        progress.setVisibility(View.GONE);
    }



    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()) {
            case R.id.img_refresh: {
                refresh();
                break;
            }
        }
    }

    public void refresh(){
        RendaFixaRest rest = new RendaFixaRest(getActivity());
        if(rest.enviar()){
            progress.setVisibility(View.VISIBLE);
            lvRendaFixa.setVisibility(View.GONE);
        }
    }

    private void onClickItem(int position){

        RendaFixaPostsObj rendaFixaPostsObj = (RendaFixaPostsObj)lvRendaFixa.getItemAtPosition(position);

        if(rendaFixaPostsObj != null && rendaFixaPostsObj.attachments != null && rendaFixaPostsObj.attachments.length > 0 && !Utils.ehVazio(rendaFixaPostsObj.attachments[0].url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rendaFixaPostsObj.attachments[0].url.toString().trim()));
            startActivity(browserIntent);
        }else {
            Toast.makeText(this.getActivity(), "Nenhum arquivo encontrado!", Toast.LENGTH_LONG).show();
        }
    }

}