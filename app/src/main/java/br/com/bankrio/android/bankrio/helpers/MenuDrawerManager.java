package br.com.bankrio.android.bankrio.helpers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.bankrio.android.bankrio.BankRioApplication;
import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.activity.AplicativoActivity;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.IndicesEvent;
import br.com.bankrio.android.bankrio.fragment.BlogFragment;
import br.com.bankrio.android.bankrio.fragment.FaleConoscoFragment;
import br.com.bankrio.android.bankrio.fragment.FormulariosFragment;
import br.com.bankrio.android.bankrio.fragment.HomeFragment;
import br.com.bankrio.android.bankrio.fragment.indices.IndicesFragment;
import br.com.bankrio.android.bankrio.fragment.PerfilInvestidorFragment;
import br.com.bankrio.android.bankrio.fragment.RelatorioFragment;
import br.com.bankrio.android.bankrio.fragment.RendaFixaFragment;
import br.com.bankrio.android.bankrio.fragment.SobreFragment;
import br.com.bankrio.android.bankrio.fragment.TopFundosFragment;
import br.com.bankrio.android.bankrio.fragment.VideosFragment;
import br.com.bankrio.android.bankrio.fragment.simuladores.SimuladoresFragment;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 21/02/16.
 */
public class MenuDrawerManager extends SuperActivity {

    private DrawerLayout mDrawerLayout;
    protected EventBus bus;
    private FragmentManager fragmentManager;

    private EditText edtSearch;

    protected  void initDrawerMenu(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtSearch= (EditText) findViewById(R.id.edt_search);
        edtSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    edtSearch.performClick();
                    WFRest.sendRest(MenuDrawerManager.this,JsonType.WF_ACOES, edtSearch.getText().toString());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(MenuDrawerManager.this.getCurrentFocus().getWindowToken(),0);
                    return true;
                }
                return false;
            }
        });

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

    }

    @Override
    protected void onStart() {
        super.onStart();
        managerToolbar();
    }

    protected void changeFragment(Fragment fragment){
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(fragment.getClass().toString())
                .commit();
//                .addToBackStack("xyz")
//                .commitAllowingStateLoss();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                managerToolbar();
            }
        });

    }

    public void managerToolbar(){
        SuperFragment fragment = getCurrentFragment();
        if(fragment == null){
            return;
        }

        if(fragment.getClass().equals(HomeFragment.class)){
            showLogo();
        }else if(fragment.getClass().equals(VideosFragment.class)) {
            showTitle(fragment.getTitle());
            showActionVideo();
        }else if(fragment.getClass().equals(RendaFixaFragment.class) ||
                fragment.getClass().equals(RelatorioFragment.class) ||
                fragment.getClass().equals(TopFundosFragment.class) ||
                fragment.getClass().equals(FormulariosFragment.class)) {
            showTitle(fragment.getTitle());
            showRefresh();
        }else{
            showTitle(fragment.getTitle());
        }
    }

    protected void managerDrawer(){
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }


    public void onClickMenu(View v){
        managerDrawer();
    }

    public void onClickHome(View v){
        changeFragment(new HomeFragment());
        managerDrawer();
    }

    public void onClickSobre(View v){
        changeFragment(new SobreFragment());
        managerDrawer();
    }

    public void onClickFaleConosco(View v){
        changeFragment(new FaleConoscoFragment());
        managerDrawer();
    }

    public void onClickVideos(View v){
        changeFragment(new VideosFragment());
        managerDrawer();
    }

    public void onClickBlog(View v){
        changeFragment(new BlogFragment());
        managerDrawer();
    }

    public void onClickRendaFixa(View v){
        changeFragment(new RendaFixaFragment());
        managerDrawer();
    }

    public void onClickRelatorios(View v){
        changeFragment(new RelatorioFragment());
        managerDrawer();
    }

    public void onClickFormularios(View v){
        changeFragment(new FormulariosFragment());
        managerDrawer();
    }

    public void onClickTopFundos(View v){
        changeFragment(new TopFundosFragment());
        managerDrawer();
    }

    public void onClickPerfilInvestidor(View v){
        changeFragment(new PerfilInvestidorFragment());
        managerDrawer();
    }

    public void onClickSimuladores(View v){
        changeFragment(new SimuladoresFragment());
        managerDrawer();
    }

    public void onClickIndices(View v){
        changeFragment(new IndicesFragment());
        managerDrawer();
    }

    public void onClickAplicativo(View v){
        Intent app = new Intent(this, AplicativoActivity.class);
        startActivity(app);
        managerDrawer();
    }


    public SuperFragment getCurrentFragment(){
        return (SuperFragment) fragmentManager.findFragmentById(R.id.content_frame);
    }

    /**
     * Retorna um array de aba
     * */
    public void onEvent(IndicesEvent event){
        if(event.position == 1){
            showActionSearch();
        }else{
            hideActionSearch();
            hideSearch();
        }
    }

    public void onHideSearch(View v){
        hideSearch();
    }


    public void onDeleteAllWords(View v){
        edtSearch.setText("");
    }

    public void onClickSearch(View v){
        showSearch();
    }


    public void showSearch(){
        ((RelativeLayout) findViewById(R.id.rl_search)).setVisibility(View.VISIBLE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInputFromWindow(edtSearch.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED,0);
        edtSearch.requestFocus();


    }


    public void hideSearch(){
        edtSearch.setText("");
        ((RelativeLayout) findViewById(R.id.rl_search)).setVisibility(View.GONE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),0);
        WFRest.sendRest(this, JsonType.WF_ACOES);
    }


}
