package br.com.bankrio.android.bankrio.fragment;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.MarqueeAdapter;
import br.com.bankrio.android.bankrio.adapter.VitrineSlidePagerAdapter;
import br.com.bankrio.android.bankrio.cache.Cache;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.BlogRest;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.BlogEvent;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.fragment.indices.IndicesFragment;
import br.com.bankrio.android.bankrio.fragment.simuladores.SimuladoresFragment;
import br.com.bankrio.android.bankrio.gcm.TargetNotification;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.model.BlogModel;
import br.com.bankrio.android.bankrio.obj.BlogPostObj;
import br.com.bankrio.android.bankrio.obj.webfeeder.MarqueeObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Benhur on 21/02/16.
 */
public class HomeFragment extends SuperFragment {

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    private View view;
    private View loading;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private VitrineSlidePagerAdapter mPagerAdapter;
    private EventBus bus;
    private BlogPostObj destaque;

    private RecyclerView mRecyclerView;
    private MarqueeAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private final Runnable SCROLLING_RUNNABLE = new Runnable() {

        @Override
        public void run() {
            final int duration = 10;
            final int pixelsToMove = 10;
            mRecyclerView.smoothScrollBy(pixelsToMove, 0);
            mHandler.postDelayed(this, duration);
        }
    };

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean foundTotalPixel = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int totalMovedPixel;
    private int totalPixel = 0;
    private LinearLayoutManager layoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_home, container, false);
        initView();
        return view;
    }


    private void initView() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String msg = bundle.getString("TargetNotification", "");
            if (!Utils.ehVazio(msg)) {
                changeFragment(TargetNotification.getFragment(msg));
            }
        }

        bus = EventBus.getDefault();
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) view.findViewById(R.id.vp_slide);
        loading = (View) view.findViewById(R.id.loading);


        initMarquee();
        callBlogAPI();

    }

    private void initMarquee(){
        WFRest.sendRest(getActivity(),JsonType.WF_MARQUEE);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_marquee);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickItens(View v) {
        super.onClickItens(v);
        switch (v.getId()){
            case R.id.ll_sobre: {
                changeFragment(new SobreFragment());
                break;
            }
            case R.id.ll_indices: {
                changeFragment(new IndicesFragment());
                break;
            }
            case R.id.ll_formularios:{
                changeFragment(new FormulariosFragment());
                break;
            }
            case R.id.ll_relatorios: {
                changeFragment(new RelatorioFragment());
                break;
            }
            case R.id.ll_top_fundos: {
                changeFragment(new TopFundosFragment());
                break;
            }
            case R.id.ll_renda_fixa:{
                changeFragment(new RendaFixaFragment());
                break;
            }
            case R.id.ll_calculadora: {
                changeFragment(new SimuladoresFragment());
                break;
            }
            case R.id.ll_videos: {
                changeFragment(new VideosFragment());
                break;
            }
            case R.id.ll_blog:{
                changeFragment(new BlogFragment());
                break;
            }
            default:
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }


    /**
     * Retorna um array de blogs
     * */
    public void onEvent(MarqueeObj[] marquee){
        // specify an adapter (see also next example)
        Utils.dLog("uhuuuu ===> " + marquee.length);

        mAdapter = new MarqueeAdapter(marquee);
        mRecyclerView.setAdapter(mAdapter);
        autoScrollMarquee(marquee.length);

    }

    /**
     * Retorna um array de blogs
     * */
    public void onEvent(BlogEvent event){

        loading.setVisibility(View.GONE);
        BlogPostObj[] blogs = event.blogs.posts;



        if(blogs != null){
//            for(BlogPostObj blog : blogs){
//                for (CategoriaObj categoria : blog.categories){
//                    if(categoria.title.equalsIgnoreCase("Destaque")){
//                        destaque = blog;
//                        break;
//                    }
//                }
//
//                if(destaque != null){
//                    mPagerAdapter = new VitrineSlidePagerAdapter(getActivity().getSupportFragmentManager(), destaque);
//                    mPager.setAdapter(mPagerAdapter);
//
//                    CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator_default);
//                    indicator.setViewPager(mPager);
//
//                    break;
//                }
//            }

            //get last item
            destaque = blogs[0];
            if(destaque != null){
                mPagerAdapter = new VitrineSlidePagerAdapter(getActivity().getSupportFragmentManager(), destaque);
                mPager.setAdapter(mPagerAdapter);

                CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator_default);
                indicator.setViewPager(mPager);
            }

        }
    }

    private void autoScrollMarquee(final int size){
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

//                totalPixel = totalPixel + dx;
//
//                Utils.dLog(" final ===> " + totalPixel);


                                totalMovedPixel = totalMovedPixel + dx;
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                if (foundTotalPixel) {
                    if (totalItemCount > 2) {
                        View headerView = layoutManager.getChildAt(0);
                        View itemView = layoutManager.getChildAt(1);

                        if (itemView != null && headerView != null) {
                        /*total visible scrolling part is total pixel's of total item's count and header view*/
                            totalPixel = /*-c.getTop() +*/ ((totalItemCount - 2) * itemView.getWidth()) + (1 * headerView.getWidth());
                            Log.e("Aqui ===>", "Total pixel x!" + totalPixel + " - " + totalMovedPixel);
                            foundTotalPixel = false;
                        }
                    }
                }

                //if (loading) {
                //if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                if (!foundTotalPixel && totalMovedPixel >= totalPixel) {
                    // loading = false;
                    Log.e("Aqui ===>", "Last Item Wow !");
                    Log.e("...", "totalMovedPixel !" + totalMovedPixel);

                    // use this to turn auto-scrolling off:
                    //mHandler.removeCallbacks(SCROLLING_RUNNABLE);
                    mRecyclerView.setAdapter(null);
                    mRecyclerView.setAdapter(mAdapter);
                    pastVisiblesItems = visibleItemCount = totalItemCount = 0;
                    totalMovedPixel = 0;

                }

            }
        });
        mHandler.post(SCROLLING_RUNNABLE);
    }




    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        Utils.showAlertDialog(getActivity(), error.mensage);
        loading.setVisibility(View.GONE);
    }

    private void callBlogAPI(){
        BlogRest rest = new BlogRest(getActivity());
        Cache blog = CacheManager.getBlog();
        if(blog != null && !Utils.needSync(blog.currentTime, JsonType.BLOG_REFRESH)){
            Gson gson = new Gson();
            BlogModel obj = gson.fromJson(blog.json.toString(), BlogModel.class);

            BlogEvent event = new BlogEvent();

            if(obj.posts != null && obj.posts.length > 0) {
                event.blogs = obj;
                onEvent(event);
            }

            return;
        }

        if(rest.enviar()){
            showProgress();
        }
    }

    private void showProgress(){
        final ImageView myView = (ImageView) view.findViewById(R.id.loading_gif);
        ((Animatable) myView.getDrawable()).start();
        loading.setVisibility(View.VISIBLE);
    }

}
