package br.com.bankrio.android.bankrio.fragment.simuladores;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;

/**
 * Created by Benhur on 23/04/16.
 */
public class SimuladoresFragment extends SuperFragment {

    private View view;
    private RelativeLayout rlDesejo1, rlDesejo2, rlDesejo3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_simuladores, container, false);
        initView();
        return view;
    }

    private void initView(){
        setTitle("Simuladores");

        rlDesejo1 = (RelativeLayout)view.findViewById(R.id.rl_desejo1);
        rlDesejo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                ft.replace(R.id.content_frame, new MilhaoFragment(), "simuladoresFragment");

                ft.commit();
            }
        });

        rlDesejo2 = (RelativeLayout)view.findViewById(R.id.rl_desejo2);
        rlDesejo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

                ft.replace(R.id.content_frame, new FaculdadeFragment(), "simuladoresFragment");

                ft.commit();
            }
        });

        rlDesejo3 = (RelativeLayout)view.findViewById(R.id.rl_desejo3);
        rlDesejo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

                ft.replace(R.id.content_frame, new AposentadoriaFragment(), "simuladoresFragment");

                ft.commit();
            }
        });
    }


}
