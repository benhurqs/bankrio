package br.com.bankrio.android.bankrio.comm;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.cache.CacheManager;
import br.com.bankrio.android.bankrio.events.CategoriasEvent;
import br.com.bankrio.android.bankrio.model.CategoriasModel;

/**
 * Created by Benhur on 03/03/16.
 */
public class CategoriasRest extends CoreComm {

    private CategoriasEvent event;

    public CategoriasRest(Context context) {
        super(context, RequestType.GET, R.string.categorias_url);
        event = new CategoriasEvent();
    }

    @Override
    public boolean enviar() {
        return super.enviar();
    }

    @Override
    protected void jsonArray(JSONArray json) {
        super.jsonArray(json);
    }

    @Override
    protected void jsonObject(JSONObject json) {
        super.jsonObject(json);

        Gson gson = new Gson();
        CategoriasModel obj = gson.fromJson(json.toString(), CategoriasModel.class);

        if(obj.categories != null && obj.categories.length > 0) {
            event.categorias = obj.categories;
            bus.post(event);

            CacheManager.saveCategories(json.toString());
        }else{
            error.mensage = context.getResources().getString(R.string.no_found);
            bus.post(error);
        }
    }
}
