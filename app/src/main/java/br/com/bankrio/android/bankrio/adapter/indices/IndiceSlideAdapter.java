package br.com.bankrio.android.bankrio.adapter.indices;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.Arrays;
import java.util.List;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.fragment.indices.AcoesFragment;
import br.com.bankrio.android.bankrio.fragment.indices.BolsasFragment;
import br.com.bankrio.android.bankrio.fragment.indices.CambioFragment;
import br.com.bankrio.android.bankrio.fragment.indices.IndiceTableFragment;

/**
 * Created by Benhur on 25/05/16.
 */
public class IndiceSlideAdapter extends FragmentStatePagerAdapter {

    private List<String> titles = null;
    private Bundle bundle = null;

    public IndiceSlideAdapter(Context ctx, FragmentManager fm, Bundle bundle) {
        super(fm);
        titles = Arrays.asList(ctx.getResources().getStringArray(R.array.indice_tabs));
        this.bundle = bundle;
    }

    public Fragment setBundleToFragment(Fragment fragment){
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return setBundleToFragment(new BolsasFragment());
            case 1:
                return setBundleToFragment(new AcoesFragment());
            case 2:
                return setBundleToFragment(new IndiceTableFragment());
            default:
                return setBundleToFragment(new CambioFragment());
        }
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position).toString();
    }
}
