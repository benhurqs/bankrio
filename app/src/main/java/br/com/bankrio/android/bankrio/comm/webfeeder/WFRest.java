package br.com.bankrio.android.bankrio.comm.webfeeder;

import android.content.Context;

import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.marquee.MarqueeRest;
import br.com.bankrio.android.bankrio.utils.Utils;

/**
 * Created by Benhur on 04/06/16.
 */
public class WFRest {

    public static boolean sendRest(final Context ctx,final int type){
//        SignInRest rest = new SignInRest(ctx, new SignInRest.WFsignListener() {
//            @Override
//            public void response(boolean success) {
//                send(ctx,type);
//            }
//        });
//        return (rest.enviar());
       return WFRest.sendRest(ctx,type,null);
    }

    public static boolean sendRest(final Context ctx, final int type, final String sigla){
        SignInRest rest = new SignInRest(ctx, new SignInRest.WFsignListener() {
            @Override
            public void response(boolean success) {
                send(ctx,type,sigla);
            }
        });
        return (rest.enviar());
    }

    private static void send(Context ctx, int type, String sigla){

        switch (type){
            case JsonType.WF_CAMBIO:
                CambioRest rest = new CambioRest(ctx);
                rest.enviar();
                break;
            case JsonType.WF_ACOES:
                if(Utils.ehVazio(sigla)) {
                    AcoesRest acoes = new AcoesRest(ctx);
                    acoes.enviar();
                }else{
                    AcoesSearchRest acoesSearchRest = new AcoesSearchRest(ctx,sigla);
                    acoesSearchRest.enviar();
                }
                break;
            case JsonType.WF_BOLSAS:
                BolsasRest bolsas = new BolsasRest(ctx);
                bolsas.enviar();
                break;
            case JsonType.WF_HISTORICO:
                HistoricoRest historicoRest = new HistoricoRest(ctx, sigla);
                historicoRest.enviar();
                break;
            case JsonType.WF_INTRADAY:
                IntradayRest intraday = new IntradayRest(ctx, sigla);
                intraday.enviar();
                break;
            case JsonType.WF_INDICES:
                CommoditiesRest commoditiesRest = new CommoditiesRest(ctx);
                commoditiesRest.enviar();

                IndicadoresRest indicadoresRest = new IndicadoresRest(ctx);
                indicadoresRest.enviar();

                InflacaoRest inflacaoRest = new InflacaoRest(ctx);
                inflacaoRest.enviar();
                break;
            case JsonType.WF_MARQUEE:
                MarqueeRest marqueeRest = new MarqueeRest(ctx);
                marqueeRest.enviar();

            default:
                break;
        }

    }

}
