package br.com.bankrio.android.bankrio.events;

import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;

/**
 * Created by Benhur on 17/06/16.
 */
public class IndicadoresEvent {

    public CambioObj[] indicadores;
    public String error;
}
