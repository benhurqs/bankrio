package br.com.bankrio.android.bankrio.obj.webfeeder;

/**
 * Created by Benhur on 25/05/16.
 */
public class CambioObj {

    /***
    *{
    "symbol": "dolcm",
    "timeUpdate": "25-05-2016 02:09:56",
    "dateTrade": "24-05-2016 00:00:00",
    "lastTrade": 3.575,
    "previous": 3.574,
    "change": 0.027990341,
    "changeMonth": 2.4942756,
    "bid": 3.573,
    "ask": 3.575,
    "timeLastTrade": "24-05-2016 17:01:52",
    "dateTradeObj": "May 24, 2016 12:00:00 AM",
    "volumeAmount": 0,
    "volumeFinancier": 0,
    "high": 3.589,
    "low": 3.543,
    "open": 3.556,
    "lastTradeLastWeek": 3.574,
    "lastTradeLastMonth": 3.488,
    "lastTradeLastYear": 4.035,
    "tickSize": 4,
    "timeLastTradeSting": "170152",
    "dateLastTradeString": "20160524"
  },*/

    public String symbol;
    public String timeUpdate;
    public String dateTrade;
    public float lastTrade;
    public float previous;
    public float change;
    public float changeMonth;
    public float bid;
    public float ask;
    public String timeLastTrade;
    public String dateTradeObj;
//    public String volumeAmount;
//    public String volumeFinancier;
    public float high;
    public float low;
    public float open;
    public float lastTradeLastWeek;
    public float lastTradeLastMonth;
    public float lastTradeLastYear;
    public float tickSize;
    public String timeLastTradeSting;
    public String dateLastTradeString;

    public String getVariacao(){
        return String.format("%.2f", change*100).replace(".",",");
    }

    public String getCotacao(){
        return String.format("%.2f", bid).replace(".",",");
    }


}
