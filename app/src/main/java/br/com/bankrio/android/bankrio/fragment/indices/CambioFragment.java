package br.com.bankrio.android.bankrio.fragment.indices;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.Arrays;

import br.com.bankrio.android.bankrio.R;
import br.com.bankrio.android.bankrio.adapter.indices.CambioAdapter;
import br.com.bankrio.android.bankrio.cache.JsonType;
import br.com.bankrio.android.bankrio.comm.webfeeder.WFRest;
import br.com.bankrio.android.bankrio.events.ErrorEvent;
import br.com.bankrio.android.bankrio.helpers.SuperFragment;
import br.com.bankrio.android.bankrio.obj.webfeeder.CambioObj;
import br.com.bankrio.android.bankrio.utils.Utils;
import de.greenrobot.event.EventBus;

/**
 * Created by Benhur on 25/05/16.
 */
public class CambioFragment extends SuperFragment {

    private View view;
    private ListView lvIndices;
    private EventBus bus;
    private CambioAdapter indiceTableAdapter;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_indice_cambio, container, false);
        initView();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    /**
     * Retorna um array de cambio
     * */
    public void onEvent(CambioObj[] event){
        indiceTableAdapter = new CambioAdapter(getActivity(), Arrays.asList(event));
        lvIndices.setAdapter(indiceTableAdapter);
        progress.setVisibility(View.GONE);
    }

    /**
     * Retorna uma mensagem de erro,
     * caso não tenha obtido sucesso no request
     * */
    public void onEvent(ErrorEvent error){
        progress.setVisibility(View.GONE);
        Utils.showAlertDialog(getActivity(), error.mensage);
        Utils.dLog("error request - " + error.mensage);
    }

    private void initView(){
        bus = EventBus.getDefault();
        progress = (ProgressBar)view.findViewById(R.id.progress);

        lvIndices = (ListView)view.findViewById(R.id.lv_indices);

        callAPI();
    }

    private void callAPI(){
        if(WFRest.sendRest(getActivity(), JsonType.WF_CAMBIO)){
            progress.setVisibility(View.VISIBLE);
        }
    }
}
